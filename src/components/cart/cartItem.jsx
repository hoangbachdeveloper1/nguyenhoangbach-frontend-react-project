import { useState } from 'react';

function CartItem({ item }) {
    const [count, setCount] = useState(1);

    const decrement = () => {
        if (count > 1) {
            setCount(count - 1);
        }
    };

    const increment = () => {
        setCount(count + 1);
    };

    return (
        <tr>
            <td>
                <a href="#" className="cart__remove">
                    <i className="fal fa-times"></i>
                </a>
            </td>
            <td>
                <img src={item.image} alt="cart-product-img" />
            </td>
            <td>{item.name}</td>
            <td>{item.price} VNĐ </td>
            <td>
                <div className="cart__group">
                    <button onClick={decrement} className="cart__button cart__button--decrement">
                        <i className="fas fa-minus"></i>
                    </button>
                    <input type="text" className="cart__count" value={count} required="" name="count[]" />
                    <button onClick={increment} className="cart__button cart__button--increment">
                        <i className="fas fa-plus"></i>
                    </button>
                </div>
            </td>
            <td>20.000.000</td>
        </tr>
    );
}

export default CartItem;
