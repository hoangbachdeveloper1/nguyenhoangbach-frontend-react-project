import { Link } from 'react-router-dom';
import Breadcrumb from '../breadcrumb/breadcrumb';
import { productList } from '../../const/products/products';
import CartItem from './cartItem';

function CartPage() {
    return (
        <div className="cart">
            <Breadcrumb title={'Giỏ hàng của bạn'} />

            <div className=" container ">
                <div className="cart__table">
                    <table className="table ">
                        <thead>
                            <tr>
                                <th>Hành động</th>
                                <th>Hình ảnh</th>
                                <th>Tên Sản Phẩm</th>
                                <th>Đơn Giá (VND)</th>
                                <th>Số lượng</th>
                                <th>Tổng (VND)</th>
                            </tr>
                        </thead>

                        <tbody>
                            {productList.limit(3).map((item) => (
                                <CartItem item={item} />
                            ))}

                            <tr>
                                <td colspan="3">
                                    <div className="cart__group">
                                        <input
                                            type="text"
                                            name="coupon_code"
                                            placeholder="Mã giảm giá"
                                            className="cart__input"
                                        />
                                        <button type="submit" className="btn btn--primary btn--large">
                                            Áp dụng
                                        </button>
                                    </div>
                                </td>
                                <td colspan="3" className="cart__submit">
                                    <button type="submit" className="btn btn--primary btn--large">
                                        Cập nhật giỏ hàng
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div className="cart__checkout container">
                <div className="row">
                    <div className="col-lg-6"> </div>
                    <div className="col-lg-6">
                        <div className="cart__table">
                            <table className="table table--modify">
                                <tbody>
                                    <tr>
                                        <th className="cart__table-title" colspan="2">
                                            Tổng giỏ hàng
                                        </th>
                                    </tr>
                                    <tr>
                                        <td className="cart__table-title">Tổng số phụ (VNĐ)</td>
                                        <td className="cart__table-content">40.000.000</td>
                                    </tr>
                                    <tr>
                                        <td className="cart__table-title">Tổng (VNĐ)</td>
                                        <td className="cart__table-content">40.000.000</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" className="cart__table-title">
                                            <Link to = "/checkout"
                                                href="checkout"
                                                className="btn btn--primary btn--large btn-fgc--small"
                                            >
                                                Thanh toán đơn hàng
                                            </Link>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div className="cart__checkout"></div>
        </div>
    );
}

export default CartPage;
