function Pagination({ pages, setPage, currentPage }) {

    // pages is total number of pages
    // setPage is  a function that control method in parent component
    // curentPage is a props from parent component


    const paginationClick = (e) => {
        setPage(e.target.dataset.page);
        console.log(`currentPage: ${currentPage}`)
        console.log(`pageNumber: ${e.target.dataset.page}`)
    };

    const pageNumbers = Array.from({ length: pages }, (_, index) => index + 1);

    return (
        <>
            <ul className="pagination">
            {console.log(`currentPage: ${currentPage}`)}
                {pageNumbers.map((pageNumber) => {
                    return (
                        <li key={pageNumber} className={`pagination__item`}>
                            <button
                                className={`pagination__button ${currentPage === pageNumber ? 'active' : ''} `}
                                onClick={paginationClick}
                                data-page={pageNumber}
                            >
                                <span>{pageNumber}</span>
                            </button>
                        </li>
                    );
                })}
            </ul>
        </>
    );
}

export default Pagination;
