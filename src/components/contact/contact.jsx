/* eslint-disable jsx-a11y/iframe-has-title */
import { useEffect, useState } from 'react';
import Breadcrumb from '../breadcrumb/breadcrumb';
import axios from 'axios';
import { masterUrl, paths } from '../../const/baseUrl/baseUrl';

function ContactPage() {

    const [contactList, setContactList] = useState([])
    
    useEffect(()=> {
        axios.get(`${masterUrl}${paths.contact}`)
        .then(response => setContactList(response.data))
        .catch(err => {throw new Error(err)})
        return ()=> {
            setContactList([])
        }
    }, [])

    return (
        <div className="contact">
            <Breadcrumb title={'Liên hệ'} />
            <div className="contact__wraps">
                <div className="contact__container container">
                    <div className="contact__row row">
                        <div className="contact__left col-md-8">
                            <div className="contact__title">
                                <h2>Liên hệ ngay với chúng tôi</h2>

                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                    unknown printer took a galley of type and scrambled it to make a type specimen book.
                                    It has survived not only five centuries, but also the leap into electronic
                                    typesetting, remaining essentially unchanged. It was popularised in the 1960s with
                                    the release of Letraset sheets containing Lorem Ipsum passages, and more recently
                                    with desktop publishing software like Aldus PageMaker including versions of Lorem
                                    Ipsum.
                                </p>
                            </div>

                            <div className="contact__form">
                                <form action="#" method="post">
                                    {/* Form input name start */}
                                    <div className="contact__form__group form-group">
                                        <input
                                            className="contact__form__control form-control"
                                            placeholder="Tên của bạn"
                                            value=""
                                            type="text"
                                            onChange={() => {}}
                                        />
                                    </div>
                                    {/* Form input name end */}

                                    {/* Form input email start */}
                                    <div className="contact__form__group form-group">
                                        <input
                                            className="contact__form__control form-control"
                                            placeholder="Email của bạn"
                                            value=""
                                            type="email"
                                            onChange={() => {}}
                                        />
                                    </div>
                                    {/* Form input email end */}

                                    {/* Form input topic start */}
                                    <div className="contact__form__group form-group">
                                        <input
                                            className="contact__form__control form-control"
                                            placeholder="Chủ đề"
                                            value=""
                                            type="text"
                                            onChange={() => {}}
                                        />
                                    </div>
                                    {/* Form input topic end */}

                                    {/* Form input theme start */}
                                    <div className="contact__form__group form-group">
                                        <textarea
                                            className="contact__form__control form-control"
                                            name="message"
                                            placeholder="Nội dung tin nhắn"
                                            required=""
                                        ></textarea>
                                    </div>
                                    {/* Form input theme end */}

                                    {/* Form input button start */}
                                    <div className="contact__form__group form-group">
                                        <button className="btn btn--large btn--primary" type="submit">
                                            Gửi thông tin
                                        </button>
                                    </div>
                                    {/* Form input button end */}
                                </form>
                            </div>
                        </div>

                        <div className="contact__right col-md-4">
                            <div className="contact__title">
                                <h2>Thông tin liên hệ</h2>
                            </div>
                            {
                                contactList.map((contact)=> {
                                    return  <div key={contact.id} className="contact__right__group">
                                                <h4>{contact.title}</h4>
                                                <p>
                                                   {contact.content}
                                                </p>
                                            </div>
                                })
                            }
                            

                          
                        </div>
                    </div>
                </div>

                <div className="contact__map">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3779.82065441442!2d105.7051569760827!3d18.672042282451116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3139cdc689cc3d1d%3A0x4c39c1211911a5db!2zVHLGsOG7nW5nIMSR4bqhaSBo4buNYyBTxrAgUGjhuqFtIEvhu7kgVGh14bqtdCBWaW5o!5e0!3m2!1svi!2s!4v1696560182792!5m2!1svi!2s"
                        style={{ border: 0, width: '100%', minHeight: '450px', height: '50vh' }}
                        allowFullScreen=""
                        loading="lazy"
                        referrerPolicy="no-referrer-when-downgrade"
                    ></iframe>
                </div>
            </div>
        </div>
    );
}

export default ContactPage;
