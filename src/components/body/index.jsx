import { useEffect, useState } from "react";

import RoutesComponents from "../routes/routesComponent";

function BodyComponent() {

    const [scroll, setScroll] = useState(0)
    const [active, setActive] = useState(false)

    const handleSetScroll =()=> {
        setScroll(window.scrollY)
    }

    // window.addEventListener("scroll", handleSetScroll)

    // const handleScrollTop = ()=> {
    //     window.scrollTo(0,0)
    // }

    
    // useEffect(()=> {
    //     if(scroll >= 100) {
    //         setActive((true))
    //     }
    //     else {
    //         setActive(false)
    //     }

    // }, [scroll])

    

    return (    <div id="body" >
                    <div className="body">
                        <RoutesComponents/>
                        {/* <div className={active ? "go-to-top active" : ''} onClick={handleScrollTop}>
                            <i className="fas fa-arrow-alt-to-top"></i>
                        </div> */}
                    </div>
                </div> );
}

export default BodyComponent;