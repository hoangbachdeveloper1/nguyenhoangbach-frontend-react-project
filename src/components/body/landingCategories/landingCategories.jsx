import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { masterUrl, paths } from '../../../const/baseUrl/baseUrl';

function LandingCategories() {
    const [outStandings, setOutStandings] = useState([]);

    useEffect(() => {
        axios
            .get(`${masterUrl}${paths.categoryOutstanding}`)
            .then((response) => setOutStandings(response.data))
            .catch((err) => {
                throw new Error(err.message);
            });
        return () => {
            setOutStandings([]);
        };
    }, []);

    return (
        <div className="landing-categories">
            <div className="row">
                {outStandings.map((outStanding, index) => {
                    return (
                        <div key={index} className="col-md-6 col-lg-6 col-sm-6">
                            <div className="landing-categories__item">
                                <div className="landing-categories__image">
                                <img src={outStanding.image || "https://upload.wikimedia.org/wikipedia/commons/a/ab/Apple-logo.png" } alt=''/>
                                </div>
                                <div className="landing-categories__title">
                                    <h3>{outStanding.name}</h3>
                                    <Link to="#">
                                        <span>Xem ngay +</span>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

export default LandingCategories;
