function LandingRatingItem(props) {
    
    const {name, avata, evaluate} = {...props}
    
    return (
        <div className="landing-rating__item">
             <div className="landing-rating__inner">
                <div className="landing-rating__content">
                    <ul className="landing-rating__stars">
                        <li>
                            <i className="fas fa-star"></i>
                        </li>
                        <li>
                            <i className="fas fa-star"></i>
                        </li>
                        <li>
                            <i className="fas fa-star"></i>
                        </li>
                        <li>
                            <i className="fas fa-star"></i>
                        </li>
                        <li>
                            <i className="fas fa-star"></i>
                        </li>
                    </ul>
                    <i className="landing-rating__paragraph">
                        {evaluate}
                    </i>
                    <div className="landing-rating__img">
                        <img src={"https://upload.wikimedia.org/wikipedia/commons/a/ab/Apple-logo.png"} alt="custimer-image"/>
                    </div>
                </div>

                <div className="landing-rating__text">
                    <div className="landing-rating__name">
                        <h3>
                            {name}
                        </h3>
                    </div>
                    <div className="landing-rating__job">
                        <p>
                            {}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default LandingRatingItem;
