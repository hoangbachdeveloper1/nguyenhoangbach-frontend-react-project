import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { masterUrl, paths } from "../../../const/baseUrl/baseUrl";
import LandingRatingItem from './landingRatingitem';
import { useEffect, useState } from "react";
import axios from "axios";
// import { customerRating } from '../../../const/customerRating/customerRating';

function LandingRating() {

    const settings = {
        className: 'landing-rating__container',
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        infinite: true,
        auto: true,
        speed: 500,
        dots: true,
        rows: 1,
        responsive: [
          {
            breakpoint: 1400,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              // centerMode: true
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      };

    const [customerRating, setCustomerRating] = useState([])

    useEffect(()=> {
      axios.get(`${masterUrl}${paths.customerEvaluate}`)
      .then((response)=>setCustomerRating(response.data))
      .catch(err=> {throw new Error(err)})
    }, [])

    return (
        <div className="landing-rating">
            <div className="landing-rating__wraps container">
            
                    <Slider {...settings}>
                        {customerRating.map((user) =>  {                   
                            return <LandingRatingItem key={user.id} 
                            name={user.name} 
                            avata={user.avata}
                            evaluate = {user.customer_evaluate[0]?.content}
                           />
                        })}
                    </Slider>
              
            </div>
        </div>
    );
}

export default LandingRating;
