import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from "react-slick";
import { masterUrl, paths } from '../../../const/baseUrl/baseUrl';
import PartnerItem from './partnerItem';
import { useEffect, useState } from 'react';
import axios from 'axios';

function LandingPartner() {

    const [brandsImage, setBrandsImage] = useState([])

    useEffect(()=> {
        axios.get(`${masterUrl}${paths.brandsImage}`)
            .then(response=> setBrandsImage(response.data))
    }, [])

    const settings = {
        className: 'landing-partner__wrasp container',
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: false,
        infinite: true,
        autoplaySpeed: 500
    };

    return (
        <div className="landing-partner">
            <Slider {...settings}>
                {brandsImage.map((brand) => (
                    <PartnerItem key={brand.brand_id} brand={brand} />
                ))}
            </Slider>
        </div>
    );
}

export default LandingPartner;
