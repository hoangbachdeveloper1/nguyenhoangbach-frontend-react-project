import { Link } from "react-router-dom";

function PartnerItem({brand}) {
    return ( <div className="landing-partner__item">
        <div className="landing-partner__link">
            <Link to={brand.link}>
                <img src={brand.image} alt={brand.name}/>
            </Link>
        </div>
    </div> );
}

export default PartnerItem;