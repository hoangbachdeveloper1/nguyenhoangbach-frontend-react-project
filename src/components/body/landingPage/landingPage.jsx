import Slide from '../slide/slide'
import PanelComponent from '../panel/panelComponents';
import LandingCategories from '../landingCategories/landingCategories';
import LandingServices from '../landingServices/landingServices';
import LandingTrend from '../landingTrend/landingTrend';
import LandingRating from '../landingRating/landingRating';
import LandingNews from '../landingNews/landingNews';
import LandingPartner from '../landingPartner/landingPartner';

function LandingPage() {
    return (    <div className="landing-page">
                    {/* Big Slide Start */}
                    <Slide/>
                    {/* Big Slide End */}

                    {/* Categories Start */}
                    <PanelComponent 
                        title="THƯƠNG HIỆU NỔI BẬT" 
                        children={<LandingCategories/>}
                        />
                    {/* Categories End */}

                    {/* Services Start */}
                      <PanelComponent 
                        title="Dịch vụ của chúng tôi"
                        children={<LandingServices/>}
                         />
                    {/* Services End */}


                    {/* Collection Start */}
                    <PanelComponent 
                        title="Bộ sưu tập xu hướng" 
                        children={<LandingTrend/>}
                        />
                    {/* Collection End */}

                    {/* Ratting Start */}
                     <PanelComponent 
                        title="Đánh giá của khách hàng" 
                        children={<LandingRating/>}
                        />
                    {/* Ratting End */}

                    {/* Blog Start */}
                     {/* <PanelComponent 
                        title="Bài viết nổi bật" 
                        children={<LandingNews/>}
                        /> */}
                    {/* Blog End */}

                    {/* Partner Start */}
                     <PanelComponent 
                        title="Đối tác của chúng tôi" 
                            children={<LandingPartner/>}
                        />
                    {/* Partner End */}

                </div>);
}

export default LandingPage;