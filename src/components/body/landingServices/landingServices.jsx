// import { services } from '../../../const/services/services';

import axios from "axios";
import { useEffect, useState } from "react";
import { masterUrl, paths } from "../../../const/baseUrl/baseUrl";

function LandingServices() {

    const [services, setServices] = useState([])
    
    useEffect(()=> {
        axios.get(`${masterUrl}${paths.services}`)
        .then((response)=>setServices(response.data))
        .catch((error)=>{throw new Error(error)})
        return ()=> {
            setServices([])
        }
     }, [])


    return (
        <div className="landing-services">
            <div className="row">
                {services.map((service) => {
                    return <div key={service.id}  className="col-lg-3 col-sm-6">
                        <div className="landing-services__item">
                            <div className="landing-services__wraps">

                                <div className="landing-services__image">
                                    <img src={service.image} alt="services-icon" />
                                </div>
                            
                                <div className="landing-services__text">
                                    <h3>{service.name}</h3>
                                    <p>{service.description}</p>
                                </div>
                                
                            </div>
                        </div>
                    </div>;
                })}
            </div>
        </div>
    );
}

export default LandingServices;
