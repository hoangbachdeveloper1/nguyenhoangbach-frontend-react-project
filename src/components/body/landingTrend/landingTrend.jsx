import { collects } from '../../../const/collectTrend/collectTrend';
import ProductList from '../../products/productList';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { masterUrl, paths } from '../../../const/baseUrl/baseUrl';


function LandingTrend() {

    const [colection, setCollection] = useState([]);

    const [idTab, setIdTab] = useState('/products-new');

    useEffect(() => {
        axios.get(`http://localhost:8000/api/products-new`)
            .then((response) => {
                
                setCollection(response.data.data)
            }
                )
            .catch(err => {throw new Error(err)})
            return  ()=> {
                setCollection([])
            }
    }, [idTab]);

    return (
        <div className="landing-trend">
            <div className="landing-trend__navs">
                <ul className="landing-trend__navs__list">
                    {collects.map((collect) => {
                        return (
                        <li key={collect.id} className="landing-trend__navs__item " onClick={()=> {setIdTab(collect.link)}}>
                            <button
                                onClick={()=> {setIdTab(collect.link)}}
                                className={idTab === collect.link ? "landing-trend__navs__link active" : 'landing-trend__navs__link'  }
                            >
                                {collect.name}
                            </button>
                        </li>
                    )
                    })}

                  
                </ul>

                <div className="landing-trend__tabs">
                   {
                    colection.length ?  <ProductList 
                                        productList={colection} 
                                            /> 
                        :
                        ''
                   }
                    
                </div> 
            </div>
        </div>
    );
}

export default LandingTrend;
