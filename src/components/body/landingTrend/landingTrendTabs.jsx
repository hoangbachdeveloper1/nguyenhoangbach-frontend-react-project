function LandingTrendTab({ collect, children }) {
    return (
        <div
            className={collect.active ? 'tab-pane fade show active' : 'tab-pane fade'}
            id={collect.idText}
            role="tabpanel"
            aria-labelledby={collect.idText + "-tab"}
        >
            { children }
        </div>
    );
}

export default LandingTrendTab;
