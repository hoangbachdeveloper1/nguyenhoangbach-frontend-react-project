function LandingTrendNav({ idText, name, active }) {
    return (
        <li className="landing-trend__navs__item nav-item" role="presentation">
            <button
                className={active ? 'landing-trend__navs__link nav-link active' : 'landing-trend__navs__link nav-link'}
                id={idText+"-tab"}
                data-bs-toggle="tab"
                data-bs-target={'#' + idText}
                type="button"
                role="tab"
                aria-controls={idText}
                aria-selected={active}
            >
                {name}
            </button>
        </li>
    );
}

export default LandingTrendNav;
