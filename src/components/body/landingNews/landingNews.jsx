import axios from 'axios';
import NewsItem from '../../news/newsItem';
import { useEffect, useState } from 'react';
import { masterUrl, paths } from '../../../const/baseUrl/baseUrl';
function LandingNews() {


    const [newsList, setNewsList] = useState([])

    useEffect(()=> {

        axios.get(`${masterUrl}${paths.news}`)
            .then(response=> setNewsList(response.data.data))
            .catch(err=> {throw new Error(err.message)})
        return ()=> {
            setNewsList([])
        }
    }, [])


    return (
        <div className="landing-news">
            <div className="landing-news__container container">
                <div className="row">
                    <div className=""></div>
                    {newsList.limit(3).map((news) => (
                        <NewsItem key={news.id} 
                             title={news.title} 
                                thumbnail = {news.thumbnail}
                                description = {news.description}
                                user={news.user}
                                date = {new Date(news.created_at)}
                        />
                    ))}
                </div>
            </div>
        </div>
    );
}

export default LandingNews;
