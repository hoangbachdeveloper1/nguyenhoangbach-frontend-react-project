function PanelComponent({ title, children }) {
    return (
        <div className="panel">
            <div className="panel__wraps">
                <div className="panel__title">
                    <h2>{title}</h2>
                </div>
                <div className="panel__container">
                    <div className="container">{children}</div>
                </div>
            </div>
        </div>
    );
}

export default PanelComponent;
