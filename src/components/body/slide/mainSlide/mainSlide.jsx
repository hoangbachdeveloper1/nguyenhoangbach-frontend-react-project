import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import SlideItem from './slideItem';
// import { slideList } from '../../../../const/slides/slides';
import { useEffect, useRef, useState } from 'react';
import axios from 'axios';
import {masterUrl, paths } from '../../../../const/baseUrl/baseUrl';


function MainSlide() {

    const [slideList, setSlideList] = useState([])

    const setting = {
        id: 'landingPageSlick',
        className: 'landing-slick__container',
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        infinite: true,
        rows: 1,
        prevArrow: '',
        autoplay: true,
        speed: 500
    };



    const ref = useRef({});

    const next = () => {
        ref.current.slickNext();
    };
    const previous = () => {
        ref.current.slickPrev();
    };

    useEffect(()=> {
        axios.get(`${masterUrl}${paths.newProduct}`)
        .then(response=> {
            setSlideList(response.data.data)
        })
        return ()=> {
            setSlideList([])
        }
    }, []);


    return (
        <div className="landing-slick">
            <div className="landing-slick__button landing-slick__button--left" onClick={previous}>
                <i className="fal fa-arrow-alt-left"></i>
            </div>
            <Slider ref={ref} {...setting}>

                {slideList.map((slide) => {
                    return (<SlideItem 
                            key={slide.id} 
                            slideTitle=  {slide.product_name}
                            slideImage={slide.product_image[0]?.image_path}
                            slidePrice = {slide.price}
                    />)
                }
                )} 
            </Slider>
            <div className="landing-slick__button landing-slick__button--right" onClick={next}>
                <i className="fal fa-arrow-alt-right"></i>
            </div>
        </div>
    );
}

export default MainSlide;
