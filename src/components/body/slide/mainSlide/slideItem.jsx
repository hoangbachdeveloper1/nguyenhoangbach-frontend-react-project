import { Link } from 'react-router-dom';

function SlideItem({ slideTitle, slideImage, slidePrice }) {
    return (
        <div className="landing-slick__item">
            <div
                className="landing-slick__box"
                style={{
                    backgroundImage: `url(${
                        slideImage ||
                        'https://media.licdn.com/dms/image/C4E12AQETGRep_JYkuA/article-cover_image-shrink_720_1280/0/1520206550517?e=2147483647&v=beta&t=dk-LW6u9G4WlQr1q8Zcotc2zpHsbAVA0DQrAgeHg6Kk'
                    }) `,
                }}
            >
                <div className="landing-slick__banner">
                    <h1>
                        {slideTitle}{' '}
                        <span>
                            {' '}
                            {Intl.NumberFormat('vi-VN', {
                                style: 'currency',
                                currency: 'VND',
                            }).format(slidePrice)}
                        </span>{' '}
                    </h1>
                    <Link to="#" className="landing-slick__link">
                        Xem ngay
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default SlideItem;
