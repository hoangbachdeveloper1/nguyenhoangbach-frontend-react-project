import { hirringNavs } from '../../const/hirring/hiringNav';

function HirringContent() {
    return (
        <div className="hirring__content">
            <div class="tab-content" id="nav-tabContent">
                {hirringNavs.map((item) => (
                    <div
                        key={item.id}
                        class={item.selected ? 'tab-pane fade show active' : 'tab-pane fade'}
                        id={item.id}
                        role="tabpanel"
                        aria-labelledby={item.id + "-tab"}
                    >
                        <h3>{item.title}</h3>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default HirringContent;
