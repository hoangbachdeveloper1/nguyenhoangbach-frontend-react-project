import { hirringNavs } from '../../const/hirring/hiringNav';

function HirringNav() {
    return (
        <div className="hirring__nav">
            <ul class="hirring__nav__list nav nav-tabs" id="myTab" role="tablist">
                {hirringNavs.map((nav) => (
                    <li key={nav.id} class="hirring__nav__item nav-item" role="presentation">
                        <button
                            class={nav.selected ? "nav-link active" : "nav-link"}
                            id={nav.id + "-tab"}
                            data-bs-toggle="tab"
                            data-bs-target={"#"+nav.id}
                            type="button"
                            role="tab"
                            aria-controls={nav.id}
                            aria-selected={nav.selected}
                        >
                            <h5>
                                {
                                    nav.title
                                }
                            </h5>
                            
                        </button>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default HirringNav;
