import Breadcrumb from '../breadcrumb/breadcrumb';
import SideBarComponent from '../sidebarComponent/sidebarComponent';
import HirringNav from './hirringNav';
import HirringContent from './hirringContent';

function HirringPage() {
    return ( <div className="hirring">
                <Breadcrumb title={"Tuyển dụng"} />
                <div className="hirring__container">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4">
                                <SideBarComponent children={<HirringNav/>} />
                            </div>
                            <div className="col-md-8">
                                <HirringContent/>
                            </div>
                        </div>
                    </div>
                </div>
    </div> );
}

export default HirringPage;