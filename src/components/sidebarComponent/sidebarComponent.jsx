function SideBarComponent({ children }) {
    return (
        <div className="sidebar">
            <div className="sidebar__container">
                <div className="sidebar__wraps">{children}</div>
            </div>
        </div>
    );
}

export default SideBarComponent;
