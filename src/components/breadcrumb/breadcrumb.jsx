import { Link } from 'react-router-dom';
import { breadcrumb } from '../../assets/images';

function Breadcrumb({title, link}) {
    return (
        <div className="breadcrumb" style={{ background: `url(https://www.appleshowroominpune.in/images/apple-showroom-pune-banner5.jpg)` }}>
            <div className="container">
                <div className="breadcrumb__container">
                    <div className="breadcrumb__title">
                        <h1>{title}</h1>
                    </div>
                    <div className="breadcrumb__description">
                        <div className="breadcrumb__description__text">
                            <Link to="/">Trang Chủ</Link> <span> - </span> <span>{title}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Breadcrumb;
