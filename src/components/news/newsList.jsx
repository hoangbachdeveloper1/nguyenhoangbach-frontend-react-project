import Breadcrumd from '../breadcrumb/breadcrumb';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { GetUserId } from '../../functions/functions';
import { masterUrl, paths } from '../../const/baseUrl/baseUrl';
import NewsListItem from './newListItem';


function NewsList() {
    const [user, setUser] = useState({});
    const [postList, setPostList] = useState([])

    useEffect(() => {
        GetUserId()
            .then((user) => {
                setUser(user)
                axios.get(`${masterUrl}${paths.newList}/${user.id}`)
                .then(response => setPostList(response.data.data))
        })
    }, []);

   

    return (
        <div className="news">
            <Breadcrumd title={'Quản lý bài viết của bạn'} link={'/news'} />
            
            <div className="news__container container">
                <div className="news__manage">
                    <div className="news__manage__user">
                  
                       
                      
                        <div className="user__name">
                             <h3> Tác giả:  {user.name}</h3>
                        </div>

                        <div className="user__avata">
                            <img src="https://scontent.fvii1-1.fna.fbcdn.net/v/t39.30808-6/411064760_1787589848429113_6059102228212130028_n.jpg?_nc_cat=101&ccb=1-7&_nc_sid=efb6e6&_nc_eui2=AeGbOJ38skpAOVCv7dQxmjEHYAkBI51wAdBgCQEjnXAB0CuECV8mkXXE_8BlZB24OruvkGQzAPE6wEG3ytB2BAfN&_nc_ohc=EA8vF3CLtVUAX9kfNFQ&_nc_ht=scontent.fvii1-1.fna&oh=00_AfAsRtEGr1IF2y4mLgK9o-1UQ4QQ_4kTgkB_rK4NIRfIiA&oe=65A9C060" />
                        </div>
                    </div>

                    <div className="news__manage__list">
                        <ul>
                            {
                                postList.map(post=> {
                                    return <NewsListItem 
                                        title = {post.title}
                                        thumbnail={post.thumbnail}
                                        date = {new Date(post.created_at)}
                                        postId = {post.id}
                                    />
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default NewsList;
