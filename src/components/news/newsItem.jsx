import { Link } from 'react-router-dom';

function NewsItem({title, thumbnail, description, user, date, id}) {
    
    return (
        <div className="news__item col-lg-4 col-md-4">
            <div className="news__item__wraps">
                <Link to="news-post" state= {{ post_id: id }}  className="news__item__top">
                    <div className="news__item__img">
                        <img src={thumbnail} alt={title} />
                    </div>
                    <div className="news__item__date">
                        <p className="news__item__day">{date.getDate()}</p>
                        <p className="news__item__mounth">{`T.${1 + date.getMonth()}`}</p>
                    </div>
                </Link>

                <div className="news__item__bottom">
                    <div className="news__item__sub-title">
                        <h3>Tin tức</h3>
                    </div>

                    <div className="news__item__title">
                        <h2>
                            <Link state= {{ post_id: id }} to="news-post">{title}</Link>
                        </h2>
                    </div>

                    <div className="news__item__description">
                        <p>{description}</p>
                    </div>

                    <div className="news__item__link">
                        <Link to="/news-post" state= {{ post_id: id }}>
                            Đọc thêm
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default NewsItem;
