import { useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import axios from 'axios';
import { masterUrl, paths } from '../../const/baseUrl/baseUrl';

function NewsSidebar() {

    const [newsList, setNewList] = useState([])

    const setting = {
        id: 'sidebar__slide__wraps',
        className: 'landing-slick__container',
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        infinite: true,
        rows: 2,
        prevArrow: '',
    };

    const ref = useRef({});

    const next = () => {
        ref.current.slickNext();
    };
    const previous = () => {
        ref.current.slickPrev();
    };

    useEffect(()=> {
        axios.get(`${masterUrl}${paths.news}`)
        .then(response => {
            setNewList(response.data.data)
        })
    }, [])


    return (
        <div className="news__sidebar">
            <div className="news__sidebar__title">
                <h3>TIN TỨC MỚI NHẤT</h3>
                <div className="news__sidebar__button">
                    <button onClick={next} className="sidebar__button sidebar__button--left ">
                        <i className="fas fa-caret-left"></i>
                    </button>

                    <button onClick={previous}  className="sidebar__button sidebar__button--right ">
                        <i className="fas fa-caret-right"></i>
                    </button>
                </div>
            </div>

            {/* Slide Start */}
            <div className="news__sidebar__slide">
                <div className="sidebar__slide">
                    <Slider {...setting} ref={ref}>
                        {/* SlideItem Start */}
                        {newsList.map((newsItem) => (
                            <div key={newsItem.id} className="sidebar__slide__item">
                                <div className="slide__item__wraps">
                                    <Link className="slide__item__img">
                                        <img src={newsItem.thumbnail} alt="slide_item-img" />
                                    </Link>

                                    <Link className="slide__item__title">
                                        <h5>{newsItem.title}</h5>
                                    </Link>

                                    <div className="slide__item__description">
                                        <p>{newsItem.desc}</p>
                                    </div>

                                    <div className="slide__item__link">
                                        <Link to="">Xem Thêm</Link>
                                    </div>
                                </div>
                            </div>
                        ))}
                        {/* SlideItem End */}
                    </Slider>
                </div>
            </div>
            {/* Slide End */}
        </div>
    );
}

export default NewsSidebar;
