import Breadcrumd from '../breadcrumb/breadcrumb';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useEffect, useState } from 'react';
import { masterUrl, paths } from '../../const/baseUrl/baseUrl';
import { GetUserId } from '../../functions/functions';
import axios from 'axios';


function CreateNews() {

    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')
    const [thumbnail, setThumbnail] = useState('')
    const [userId, setUserId] = useState('')
    const [tags, setTags] = useState('')
    const [description, setDescription] = useState('')
    
    // get user id
    useEffect(() => {
        GetUserId()
        .then(userId=> {
            setUserId(userId)
        })
      }, []);
    

    const handleDescriptionChange =(e)=> {
        setDescription(e.target.value)
    }
    
    const handleTagsChange=(e)=> {
        setTags(e.target.value)
    }

    const onHandleTitleChange = (e) => {
      setTitle(e.target.value)
    };

    const handleFileChange=(e)=> {
        setThumbnail(e.target.files[0])
    }

    const onContentChange = (e, editor) => {
        setContent(editor.getData())
    }

    const onHandlePostSubmit = (e) => {
        e.preventDefault();
        
        const _formData = new FormData()
        _formData.append('user_id', userId)
        _formData.append('title', title)
        _formData.append('content', content.toString())
        _formData.append('thumbnail', thumbnail)
        _formData.append('description', description)
        _formData.append('tags', tags)

        axios.post(`${masterUrl}${paths.news}`, _formData)
        .then((response)=> {
            alert(`Thêm bài viết ${response.data.title} thành công!`)
        })
        .catch()
    };

    return (
        <div className="news">
            <Breadcrumd title={'Tạo bài viết mới'} link={'/news'} />
            <div className="news__container container">
                <div className="news__form">
                    <form onSubmit={onHandlePostSubmit} action="#" method="POST">
                        <div className="news__form__group form-group">
                            <label htmlFor="title">Tiêu đề bài viêt</label>
                            <input
                                type="text"
                                name="title"
                                className="news__form__control form-control"
                                id="title"
                                value={title}
                                onChange={onHandleTitleChange}
                                required={true}
                            />
                        </div>

                        <div className="news__form__group form-group">
                            <label htmlFor="description">Mô tả</label>
                            <textarea
                                type="text"
                                name="description"
                                className="news__form__control form-control"
                                id="description"
                                onChange={handleDescriptionChange}
                                value={description}
                            >
                            </textarea>
                        </div>

                        <div className="news__form__group form-group">
                            <label htmlFor="thumbnail">Ảnh thu nhỏ</label>
                            <input
                                type="file"
                                name="thumbnail"
                                className="news__form__control form-control"
                                id="thumbnail"
                                onChange={handleFileChange}
                                required={true}
                            />
                        </div>

                        <div className="news__form__group form-group">
                            <label>Nội dung bài viết</label>
                            <CKEditor
                                editor={ClassicEditor}
                                data={content}
                                onChange={onContentChange}
                                className="news__form__control form-control news__form__control__ck"
                                style={{height: "400px"}}
                            />
                        </div>

                        <div className="news__form__group form-group">
                            <label htmlFor="tags">Thẻ</label>
                            <input
                                type="text"
                                name="tags"
                                className="news__form__control form-control"
                                id="tags"
                                onChange={handleTagsChange}
                                value={tags}
                            />
                        </div>

                        <div className="news__form__group form-group">
                            <input
                                type="submit"
                                className="form-control btn btn--primary btn--large"
                                value={'Lưu bài viết'}
                            />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateNews;
