import { Link } from 'react-router-dom';

function NewsRealateItem({ post }) {
    return (
        <div className="col-lg-6">
            <div className="post__relate__item">
                <div className="relate__item">
                    <div className="relate__wraps">
                        <Link to="" className="relate__image">
                            <img src={post.img} alt="relate-img" />
                        </Link>

                        <div className="relate__container">
                            <div className="relate__date">
                                <h4>26</h4>
                                <h4>07</h4>
                            </div>

                            <div className="relate__content">
                                <div className="relate__title">
                                    <h3>
                                        <Link to="">{post.title}</Link>
                                    </h3>
                                </div>
                                <div className="relate__description">
                                    <p>{post.description}</p>
                                </div>
                                <div className="relate__link">
                                    <Link>Xem thêm</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default NewsRealateItem;
