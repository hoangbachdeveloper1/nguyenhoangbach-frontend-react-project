import { Link } from 'react-router-dom';

function NewsListItem ({title, thumbnail, date, postId}) {
    return  <li className="news__manage__item">
    <Link to={null} className="news__item__link">
        <div className="news__item__img">
            <img src={thumbnail} alt= {title} />
        </div>
        <div className="news__item__title">
            <h3>
                {title}
            </h3>
            <p>
                <i className="fal fa-calendar-alt"></i> {`${date.getDate()} / ${date.getMonth() + 1} / ${date.getFullYear()}`}
            </p>
        </div>
    </Link>

    <div className="news__item__actions">
        <button className="btn btn--primary" data-postId={postId}>
            <i className="fal fa-edit"></i> 
            <span>
                Sửa
            </span>
        </button>
        <button className="btn btn--danger" data-postId={postId}>
            <i className="fal fa-trash"></i>
            <span>
                Xóa
            </span>
        </button>
    </div>
</li>
}

export default NewsListItem
