import { Link, useLocation } from 'react-router-dom';
import { newsRelateItems } from '../../const/news/newsRelate';
import Breadcrumb from '../breadcrumb/breadcrumb';
import NewsRealateItem from './newsRelateItem';
import SideBarComponent from '../sidebarComponent/sidebarComponent';
import { masterUrl, paths } from '../../const/baseUrl/baseUrl';
import NewsSidebar from './newsSidebar';
import { useState, useEffect } from 'react';
import axios from 'axios';

function NewsPostPage() {
    const location = useLocation()
    const { post_id } = location.state || 0
    const [post, setPost] =  useState({})
    const [author, setAuthor] = useState({})
    const [date, setDate]= useState(new Date())

    // get post details 
    useEffect(()=> {
        axios.get(`${masterUrl}${paths.news}/${post_id}`)
        .then(response => {
            setPost(response.data[0])
            const date = new Date(response.data[0].created_at)
            setDate(date)

            // return a user_id to get post author
            return new Promise((resolve, reject)=> {
                resolve(response.data[0].user_id)
                reject()
            })
            .then(userId => {
                axios.get(`${masterUrl}${paths.user}/${userId}`)
                    .then(data=> {
                        setAuthor(data.data[0])
                    })
            })
        })
        .catch(err=> {console.error(err)})

        return ()=> {
            setPost({})
        }
    }, [])

    return (
        
        <div className="news">
            <Breadcrumb title={post.title} />

            <div className="news__post post">
                <div className="post__container">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8">
                                <div className="post__content">
                                    <div className="post__title">
                                        <h2>{post.title}</h2>
                                    </div>

                                    <div className="post__info">
                                        <div className="post__info__element">
                                            <i className="fal fa-calendar-alt"></i>
                                            <span>{`${date.getDate()} / ${date.getMonth()} / ${date.getFullYear()}` }</span>
                                        </div>
                                        <div className="post__info__element">
                                            <i className="fas fa-user"></i>
                                            <span>{author.name}</span>
                                        </div>
                                    </div>

                                    <div className="post__content__text" dangerouslySetInnerHTML={{ __html: post.content }} />;
        
                                    <div className="post__tags">
                                        <p> <i className="fas fa-tags"></i> <span>Từ khóa</span></p> 
                                        <ul className="post__tags__list">
                                            {
                                       

                                                (post.tags?.split(', '))?.map((item)=> {
                                                    return  <li>
                                                <Link to="">
                                                    {item}
                                                </Link>
                                            </li>
                                                }) 
                                            }
                                           
                                        </ul>
                                    </div>
                                </div>
                                
                                {/* <div className="post__relate">
                                    <div className="post__relate__container">
                                        <div className="post__relate__heading">
                                            <h3>
                                                Các tin tức liên quan
                                            </h3>
                                        </div>

                                        <div className="post__relate__list">
                                                <div className="row">
                                                   {
                                                    newsRelateItems.map(item=> <NewsRealateItem key={item.id} post ={item} /> )
                                                   }
                                                </div>
                                        </div>
                                    </div>   
                                </div> */}

                            </div>
                            <div className="col-lg-4">
                                <SideBarComponent children={<NewsSidebar/>} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default NewsPostPage;
