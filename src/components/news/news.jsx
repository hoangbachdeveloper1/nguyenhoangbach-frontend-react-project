
import { useEffect, useState } from 'react';
import {masterUrl, paths } from '../../const/baseUrl/baseUrl'; 
import Breadcrumd from '../breadcrumb/breadcrumb';
import NewsItem from './newsItem';
import axios from 'axios';
import Pagination from '../pagination/pagination';


function NewsPage() {

    const [newsList, setNewsList] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [pages, setPages] = useState(0) // total pages


    useEffect (()=> {
        axios.get(`${masterUrl}${paths.news}`)
        .then((response)=> {
            setNewsList(response.data.data)
            setPages(response.data.last_page)
        })
        .catch((error)=> {throw new Error(error.message)})
        return ()=> {
            setNewsList([])
        }
    }, [])
    
    const handleSetCurrentPage = (page) => {
        setCurrentPage(page)
    }


    return <div className="news">
        <Breadcrumd title={'Tin tức'} link={'/news'} />
        <div className="news__container">
            <div className="container">
                <div className="row">
                
                        {newsList.map((news) => {
                            return <NewsItem 
                                title={news.title} 
                                thumbnail = {news.thumbnail}
                                description = {news.description}
                                user={news.user_id}
                                date = {new Date(news.created_at)}   
                                id = {news.id}        
                            />
                        }
                        )}
                    </div>
                </div>

                <Pagination 
                    pages = {pages} // number of pages
                    setPage= {handleSetCurrentPage}  // setPages function
                    currentPage = {currentPage} // curent page number
                />
        </div>
    </div>;
}

export default NewsPage;
