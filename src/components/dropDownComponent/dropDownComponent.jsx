function TippyDropDown ({children}) {
    return <>
        <div className="tippy">
            <div className="tippy__container">
                <div className="tippy__list">
                    {children}
                </div>
            </div>
        </div>
    </>
}

export default TippyDropDown