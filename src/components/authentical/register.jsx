import { Link } from 'react-router-dom';
import Breadcrumb from '../breadcrumb/breadcrumb';
import { useState, useContext, useRef } from 'react';
import  { masterUrl, paths } from '../../const/baseUrl/baseUrl';
import axios from 'axios';
import { useNavigate  } from 'react-router-dom';
import LoginContext from '../loginContext/loginContext';

function RegisterPage() {
    const navigate  = useNavigate ()
    const {setIsLoggedIn} = useContext(LoginContext)

    const [formData, setFormData] = useState({
        name: '',
        email: '',
        password: '',
        confirm:''
    })

    const handleChange =(e)=> {
        setFormData({
            ...formData,
            [e.target.name] : e.target.value,
        })
    }

    const registerRef = useRef();

    // Event submit form register
    const handleRegister =(e)=> {
        e.preventDefault();

        if(formData.password === formData.confirm) {

            // Tạo đối tượng FormData và đưa dữ liệu vào
            const formDataObject = new FormData();
    
            Object.entries(formData).forEach(([key, value]) => {
              formDataObject.append(key, value);
            });
        
            // Lấy tất cả các giá trị từ formDataObject
            const registerFormData = {};
            for (const [key, value] of formDataObject.entries()) {
              registerFormData[key] = value;
            }

            axios.post(`${masterUrl}${paths.register}`, registerFormData)
                .then((response) => {
                    // window.location.href = '/';
                    if(!localStorage.getItem('token')) {
                        const token = JSON.stringify(response.data)
                        // localStorage only contains the string type
                        localStorage.setItem('token', token) 
                        // set state Login to true
                        setIsLoggedIn(true)
                        // goback to the forward page
                        navigate(-1)
                    }
                })
                .catch(()=> {
                    alert('Đăng kí không thành công!')
                    navigate(0)
                })
        }
    }



    return (  <div className="authentic">
    <Breadcrumb title={'Đăng kí'} />

    <div className="authentic__container">
        <div className="authentic__title">
            <h2>Đăng Kí</h2>
        </div>

        <div className="authentic__form">
            <form ref={registerRef} action="#" method="post">
                <div className="form__group form__group--custom">
                    <input
                        type="email"
                        name="email"
                        className="checkout__form-control form__control--custom"
                        placeholder="Email"
                        value={formData.email}
                        onChange={handleChange}
                        required=""
                    />
                </div>
                <div className="form__group form__group--custom">
                    <input
                        type="text"
                        name="name"
                        className="checkout__form-control form__control--custom"
                        placeholder="Tên người dùng" 
                        value={formData.name}
                        onChange={handleChange}
                        required=""
                    />
                </div>

                <div className="form__group form__group--custom">
                    <input
                        type="password"
                        name="password"
                        className="checkout__form-control form__control--custom"
                        placeholder="Mật Khẩu"
                        value={formData.password}
                        onChange={handleChange}
                        required=""
                    />
                </div>

                <div className="form__group form__group--custom">
                    <input
                        type="password"
                        name="confirm"
                        className="checkout__form-control form__control--custom"
                        placeholder="Nhập lại mật Khẩu"
                        value={formData.rePassword}
                        onChange={handleChange}
                        required=""
                    />
                </div>

                <div className="form__group form__group--custom">
                    <button onClick={handleRegister} className="btn btn--primary btn--large">Đăng ki</button>
                </div>

                <div className="form__group form__group--custom">
                    <p>
                        Bạn đã có tài khoản? <Link to="/login"> Đăng nhâp </Link>
                    </p>
                </div>
            </form>
        </div>
    </div>
</div> );
}

export default RegisterPage;