import { Link } from 'react-router-dom';
import { useState, useRef, useContext } from 'react';
import { useNavigate  } from 'react-router-dom';
import  { masterUrl, paths } from '../../const/baseUrl/baseUrl';
import axios from 'axios';
import Breadcrumb from '../breadcrumb/breadcrumb';
import LoginContext from '../loginContext/loginContext';

function LoginPage() {

    const loginRef = useRef();
    const navigate  = useNavigate ()
    
    const [formData, setFormData] = useState({
        email: '',
        password: ''
    })

    const handleChange =(e)=> {
        setFormData({
            ...formData,
            [e.target.name] : e.target.value,
        })
    }

    const {setIsLoggedIn} = useContext(LoginContext)

    // this event for form submission 
    const handleLoginFormSubmit = (e) => { 
        e.preventDefault()
        // call Api to send the form data login
        axios.post(`${masterUrl}${paths.login}`,formData)
            .then(response=> {
                    if(!localStorage.getItem('token')) {
                        const token = JSON.stringify(response.data)
                        // localStorage only contains the string type
                        localStorage.setItem('token', token) 
                        // set state Login to true
                        setIsLoggedIn(true)
                        // goback to the forward page
                        navigate(-1)
                    }
                    
                })
            .catch(err => {throw new Error(err)})
            .finally()
    }

    


    return (
        <div className="authentic">
            <Breadcrumb title={'Đăng nhập'} />

            <div className="authentic__container">
                <div className="authentic__title">
                    <h2>Đăng Nhập</h2>
                </div>
                <div className="authentic__form">
                    <form onSubmit={handleLoginFormSubmit} ref={loginRef} action="#" method="get">
                        <div className="form__group form__group--custom">
                            <input
                                type="email"
                                name="email"
                                className="checkout__form-control form__control--custom"
                                placeholder="Email"
                                required
                                value={formData.email}
                                onChange={handleChange}
                            />
                        </div>

                        <div className="form__group form__group--custom">
                            <input
                                type="password"
                                name="password"
                                className="checkout__form-control form__control--custom"
                                placeholder="Mật khẩu"
                                required
                                value={formData.password}
                                onChange={handleChange}
                            />
                        </div>

                        <div className="form__group form__group--custom">
                            <input type='submit' className="btn btn--primary btn--large" value={"Đăng nhập"} />
                        </div>

                        <div className="form__group form__group--custom">
                            <p>
                                Bạn chưa có tài khoản? <Link to="/register"> Đăng kí </Link>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default LoginPage;
