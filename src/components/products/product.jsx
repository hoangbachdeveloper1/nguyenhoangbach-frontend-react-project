import { useState, useEffect } from 'react';
import ProductItem from './productItem';
import Breadcrumb from '../breadcrumb/breadcrumb';
import ProductFilterSidebar from './productFilterSidebar';
import axios from 'axios';
import {masterUrl, paths } from '../../const/baseUrl/baseUrl';
import Pagination from '../pagination/pagination';

function ProductPage() {

    const [filter, setFilter] = useState(false);
    const [productList, setProductList] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [pages, setPages] = useState(0) // total pages

    const handleFilter =()=> {
        setFilter(!filter)
    }

    useEffect(()=> {
        axios.get(`${masterUrl}${paths.product}?page=${currentPage}`)
        .then((result) => {
            setProductList(result.data.data)  
            setPages(result.data.last_page)
        })
        .catch((err) => {throw new Error(err)})

        return ()=> {
            setProductList([]) 
        }
    }, [currentPage])

    const handleSetCurrentPage = (page) => {
        setCurrentPage(page)
    }


    return (
        <div className="product">
            <Breadcrumb title={'Sản phẩm'} />

            <div className="product__container container">
            
                {/* Product Control Start */}
                <div className="product__control">
                    <div className="product__control__wraps">
                        {/* Filter Start */}
                        <div className="product__control__filter">
                            <button onClick={handleFilter} className="btn btn--primary btn--small control__filter">
                                <i className="fal fa-filter"></i>
                                <span>Lọc</span>
                            </button>
                        </div>
                        {/* Filter End */}

                        {/* Sort Start */}
                        <div className="product__control__sort">
                        
                            <div className="sort__layout">
                                <ul className="sort__layout__number">
                                    <li className="sort__layout__item">
                                        <button className="btn btn--primary btn--round">2</button>
                                    </li>
                                    <li className="sort__layout__item">
                                        <button className="btn btn--primary btn--round">3</button>
                                    </li>
                                    <li className="sort__layout__item">
                                        <button className="btn btn--primary btn--round">4</button>
                                    </li>
                                </ul>

                                <button  className="btn btn--primary btn--round">
                                    <i className="fab fa-windows"></i>
                                </button>
                            </div>

                            <div className="sort__by">
                                <select>
                                    <option value={null}>--- Sắp xếp theo ---</option>
                                    <option value={null}>--- Mặc định ---</option>
                                    <option value={null}>--- Bán chạy ---</option>
                                    <option value={null}>--- Theo Tên A - Z ---</option>
                                    <option value={null}>--- Theo Tên Z - A ---</option>
                                    <option value={null}>--- Theo Giá Cao - Thấp ---</option>
                                    <option value={null}>--- Theo Giá Thấp - Cao ---</option>
                                </select>
                            </div>
                        </div>
                        {/* Sort End */}

                    </div>
                </div>
                {/* Product Control End */}

                {/* Product List Start */}
                <div className="product__row row">

                    {
                        filter ? <ProductFilterSidebar/> : null
                    }

                    <div className= {` ${filter ? "col-md-9" : "col-md-12"} product__list`}>
                        <div className="row">
                       
                            {productList.map((product) => {
                                return  <ProductItem 
                                key={product.id} 
                                productName = {product.product_name}
                                productPrice = {product.price}
                                image={product.product_image[0]?.image_path} 
                                product_id = {product.id}
                                />
                            }
                            )}
                        </div>

                    {/* Pagination Start */}
                        <Pagination 
                            pages = {pages} // number of pages
                            setPage= {handleSetCurrentPage}  // setPages function
                            currentPage = {currentPage} // curent page number
                        />
                    </div>  
                </div>
                {/* Product List End */}
            </div>
        </div>
    );
}

export default ProductPage;
