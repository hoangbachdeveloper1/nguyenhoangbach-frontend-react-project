import { Link } from 'react-router-dom';
// import { categories } from '../../const/categories/categories';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { masterUrl, paths } from '../../const/baseUrl/baseUrl';

function ProductFilterSidebar() {
    const [fromPrice, setFromPrice] = useState(0);
    const [toPrice, setToPrice] = useState(0);
    const [categories, setCategories] = useState([])

    const handleSetFrom = (e) => {
        setFromPrice(e.target.value)
        
    };

    const handleSetTo = (e) => {
        setToPrice(e.target.value);
    };

    useEffect(()=> {

        axios.get(`${masterUrl}${paths.category}`)
            .then(response=> setCategories(response.data))
        return ()=> {
            setCategories([])
        }
    }, [])

    return (
        <div className="col-md-3">
            <div className="product__filter">

                <div className="product__filter__group">
                    <div className="group__title">
                        <h3>Danh mục</h3>
                        <span></span>
                    </div>

                    <div className="group__conditions">
                        <ul>
                            {categories.map((category) => (
                                <li key={category.category_id}>
                                    <Link to="">
                                        <i className="fal fa-album-collection"></i>
                                        {category.name}
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>

                <form className="product__filter__group">
                    <div className="group__title">
                        <h3>Giá</h3>
                        <span></span>
                    </div>

                    <div className="group__conditions">
                        <div className="group__input">
                            <div className="group__input__wraps">
                                <label htmlFor="fromPrice">Giá từ...</label>
                                <input
                                    id="fromPrice"
                                    type="number"
                                    value={fromPrice}
                                    onChange={handleSetFrom}
                                    placeholder="Từ...."
                                />
                            </div>

                            <div className="group__input__wraps">
                                <label htmlFor="toPrice">Giá đến...</label>
                                <input
                                    id="toPrice"
                                    type="number"
                                    value={toPrice}
                                    onChange={handleSetTo}
                                    placeholder="Đến...."
                                />
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    );
}

export default ProductFilterSidebar;
