import axios from 'axios';
import Breadcrumb from '../breadcrumb/breadcrumb';
import { useEffect, useState } from 'react';
import { masterUrl, paths } from '../../const/baseUrl/baseUrl';

function CreateProduct() {
    const [brands, setBrands] = useState([])
    const [categories, setCategories] = useState([])
    
    const [productImages, setProductImages] = useState([])
    
    const [productData, setProductData] = useState({
        product_name: '',
        available: 0,
        price: 0,
        description: '',
        quantity: 0,
        brand_id: -1,
        category_id: -1,
    });

    const handleFilesChange =(e)=> {
        // cập nhật lại state files 
        const imageLength = e.target.files;
        if(imageLength.length > 0) { 
            let product_image = productImages
            for (let i = 0; i < imageLength.length; i++) {
                if(e.target.files[i]) {
                    let file = e.target.files[i];
                    const reader = new FileReader();
                    // eslint-disable-next-line no-loop-func
                    reader.onloadend = async ()=> {
                        let img = reader.result
                        const index = product_image.findIndex((item) => item == img);
                        if(index === -1) {
                            product_image = [...product_image, file]
                            setProductImages(product_image)
                        }
                    }
                    reader.readAsDataURL(file)
                }
            }
        }
    }

    const handleCreateProductImage = (productId) => {
        const _formData = new FormData()
        _formData.append("product_id", productId)
        productImages.forEach((image_file) => {
            _formData.append('images[]', image_file)
            });
        console.log( _formData.getAll("images[]"))
        axios.post(`${masterUrl}${paths.productImage}`, _formData)
        .then(response => {
            console.log(response.data)
        })       
    };

    const handleProductFormSubmit = (e) => {
        e.preventDefault()
       
        if(productData.quantity > 0) {
            setProductData({
                ...productData,
                available: 1,
            })
        } else {
            setProductData({
                ...productData,
                available: 0,
            })
        }

        axios.post(`${masterUrl}${paths.product}`, productData)
        .then((response) => {
            console.log(response.data)
            alert(`Đã thêm sản phẩm ${response.data.product_name}`)
            // handleFinallyCreate()

            // return promise để sau khi thêm sản phẩm, lấy id của sản phẩm đó rồi
            // thêm (các) bản ghi trong bảng  product images
            return new Promise((resolve, reject) => {
                resolve(response.data.id)
                // nếu thành công thì trả về một cái product id
                reject()
                // thất bại thì tùy

            })
        })
        .then((productId)=> {
            // khi thêm sản phẩm thành công
            // thì lấy dữ liệu images từ thằng productData 
            // gửi request lên server thêm vào bảng product_images
            handleCreateProductImage(productId)
        })
        .catch(err=> {
            alert(`Thêm sản phẩm lỗi: Mã lỗi: ${err.message}`)
        })
    }


    const handleChangeProductData = (e) => {
        setProductData({
            ...productData,
            [e.target.name]: e.target.value,
        });
    };

    useEffect(() => {
        axios.get(`${masterUrl}${paths.category}`).then((response) => {
            setCategories(response.data);
        });
    }, []);

    useEffect(() => {
        axios.get(`${masterUrl}${paths.brand}`).then((response) => {
            setBrands(response.data);
        });
    }, []);

    const handleFinallyCreate=()=> {
        setProductData({
            product_name: '',
            available: 0,
            price: 0,
            description: '',
            quantity: 0,
            brand_id: -1,
            category_id: -1,
        })
    }

  
    return (
        <div className="product">
            <Breadcrumb title={'Tạo sản phẩm mới'} />
            <div className="product__container container">
                <div className="product__form">
                    <form onSubmit={handleProductFormSubmit} action="#" className=""  method="POST">
                        {/* Name */}
                        <div className="product__form__group form-group">
                            <label className="product__form__label" htmlFor="name">
                                Tên sản phẩm
                            </label>
                            <input
                                type="text"
                                id="name"
                                name="product_name"
                                placeholder="Tên sản phẩm"
                                className="product__form__control form-control"
                                required
                                value={productData.product_name}
                                onChange={handleChangeProductData}
                            />
                        </div>

                        {/* Description */}
                        <div className="product__form__group form-group">
                            <label className="product__form__label" htmlFor="description">
                                Mô tả sản phẩm
                            </label>
                            <textarea
                                id="description"
                                name="description"
                                placeholder="Mô tả sản phẩm"
                                className="product__form__control form-control"
                                required
                                rows="4"
                                value={productData.description}
                                onChange={handleChangeProductData}
                            ></textarea>
                        </div>

                        {/* Image */}
                        <div className="product__form__group form-group">
                            <label className="product__form__label" htmlFor="images">
                                Hình ảnh sản phẩm
                            </label>
                            <input
                                type="file"
                                id="images"
                                name="images"
                                multiple={true}
                                placeholder="hình ảnh sản phẩm"
                                className="product__form__control form-control"
                                required
                                onChange={handleFilesChange}
                            />
                        </div>
                        {/* Price */}
                        <div className="product__form__group form-group">
                            <label className="product__form__label" htmlFor="price">
                                Giá sản phẩm
                            </label>
                            <input
                                type="number"
                                id="price"
                                name="price"
                                placeholder="Giá sản phẩm"
                                className="product__form__control form-control"
                                required
                                value={productData.price}
                                onChange={handleChangeProductData}
                            />
                        </div>

                        {/* Quantity */}
                        <div className="product__form__group form-group">
                            <label className="product__form__label" htmlFor="quantity">
                                Số lượng sản phẩm
                            </label>
                            <input
                                type="number"
                                id="quantity"
                                name="quantity"
                                placeholder="Số lượng sản phẩm"
                                className="product__form__control form-control"
                                required
                                value={productData.quantity}
                                onChange={handleChangeProductData}
                            />
                        </div>

                        {/* Brand */}
                        <div className="product__form__group form-group">
                            <label className="product__form__label" htmlFor="brand">
                                Thương hiệu
                            </label>
                            <select
                                id="brand"
                                name="brand_id"
                                onChange={handleChangeProductData}
                                className="product__form__control form-control"
                                value={productData.brand_id}
                            >
                                <option value={null}>---</option>
                                {brands.map((brand) => {
                                    return (
                                        <option key={brand.brand_id} value={brand.brand_id}>
                                            {brand.brand_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>

                        {/* category */}
                        <div className="product__form__group form-group">
                            <label className="product__form__label" htmlFor="category">
                                Danh mục sản phẩm
                            </label>
                            <select
                                id="category"
                                name="category_id"
                                onChange={handleChangeProductData}
                                className="product__form__control form-control"
                                value={productData.category_id}
                            >
                                <option value={null}>---</option>
                                {categories.map((category) => {
                                    return (
                                        <option key={category.category_id} value={category.category_id}>
                                            {category.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>

                        {/* Submit */}
                        <div className="product__form__group form-group">
                            <input
                                type="submit"
                                className="form-control btn btn--primary btn--large"
                                value="Tạo sản phẩm mới"
                                
                            />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateProduct;
