import ProductItem from "./productItem";


function ProductList(productList) {
    return <div className="product">
        <div className="product__wraps">
            <div className="product__container">
                <div className="container">
                    <div className="row">
                        {
                            productList.productList.map(product => {
                                console.log(product)
                                return ( <ProductItem 
                                key={product.id} 
                                product_id = {product.id}
                                productName={product.product_name}
                                image = {product.product_image[0]?.image_path}
                                productPrice = {product.price}
                                />)
                            }
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    </div>;
}

export default ProductList;
