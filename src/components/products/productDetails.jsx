import { Link, useLocation } from 'react-router-dom';
import Breadcrumb from '../breadcrumb/breadcrumb';
import { useEffect, useState, useRef } from 'react';
import axios from 'axios';
import { masterUrl, paths } from '../../const/baseUrl/baseUrl';
import Slider from 'react-slick';
import ProductItem from './productItem';

function ProductDetails() {
    const location = useLocation();
    const { product_id } = location.state || 0;
    const [product, setProduct] = useState({});
    const [nav1, setNav1] = useState(null);
    const [nav2, setNav2] = useState(null);
    const [productImages, setProductImages] = useState([]);
    const [productRelateds, setProductRelateds] = useState([]);
    const [full, setFull] = useState(false);
    const slider1 = useRef(null);
    const slider2 = useRef(null);
    const [userId, setUserId] = useState(null);

    const handleSeefullContent = () => {
        setFull((prevState) => {
            setFull(!prevState);
        });
    };

    // get ProductDetails
    useEffect(() => {
        axios
            .get(`${masterUrl}${paths.product}/${product_id}`)
            .then((response) => {
                setProduct(response.data[0]);
                return new Promise((resolve, reject) => {
                    resolve(response.data[0].brand_id);
                    reject();
                }).then((brand_id) => {
                    axios.get(`${masterUrl}${paths.productRelatedBrand}/${brand_id}`).then((productRelatedBrand) => {
                        setProductRelateds(productRelatedBrand.data);
                    });
                });
            })
            .catch(() => {
                throw new Error('Could not find product related');
            });
        return () => {
            setProduct({});
        };
    }, []);

    // get Images of product
    useEffect(() => {
        axios
            .get(`${masterUrl}${paths.productImage}/${product_id}`)
            .then((response) => {
                setProductImages(response.data);
            })
            .catch();
        return () => {
            setProductImages([]);
        };
    }, []);

    // set Slider
    useEffect(() => {
        setNav1(slider1.current);
        setNav2(slider2.current);

        return () => {
            setNav1(null);
            setNav2(null);
        };
    }, []);

    // get UserId
    useEffect(()=> {
        const token = JSON.parse(localStorage.getItem('token'));
        if (token) {
            axios
                .get(`${masterUrl}${paths.me}`, {
                    headers: {
                        Authorization: `Bearer ${token.access_token}`,
                    },
                })
                .then((response) => {
                    setUserId(response.data.id);
                    return userId
                })
             
                .catch((err) => {
                    throw new Error(err);
                });
        }
    }, [])
    const handleBuy = () => {
        const date = new Date();
        // console.log(typeof date.getTime())
        // const fullDate = `${date.getFullYear()} - ${date.getMonth()+1} ${date.getDate()}`
        axios.post(`${masterUrl}${paths.bill}`, {
            user_id: userId,
            productId: product_id,
            employee_id: 1,
            total: product.price,
            discount: 0.2,
            cash: product.price * 0.2
        })
        .then((response) => {
            console.log(response.data)
        })
    };

    return (
        <>
            <div className="product">
                <Breadcrumb title={'Chi tiết sản phẩm'} />

                <div className="product__container container">
                    <div className="product__details">
                        <div className="product__details__layout">
                            <div className="row">
                                {/* Image Start */}
                                <div className="col-lg-6">
                                    <div className="product__details__image">
                                        <div className="product__details__name">
                                            <h3>{product.product_name}</h3>
                                        </div>

                                        <div className="product__details__slider">
                                            {/* Big Image Start */}
                                            <Slider className="slider__nav" asNavFor={nav2} ref={slider1}>
                                                {productImages.map((productImage) => {
                                                    return (
                                                        <div key={productImage.id} className="slider__nav__main">
                                                            <img
                                                                key={productImage.id}
                                                                src={productImage.image_path || ''}
                                                                alt={product.product_name}
                                                            />
                                                        </div>
                                                    );
                                                })}
                                            </Slider>
                                            {/* Big Image End */}

                                            {/* Small Images Start */}
                                            <Slider
                                                asNavFor={nav1}
                                                ref={slider2}
                                                slidesToShow={3}
                                                swipeToSlide={true}
                                                focusOnSelect={true}
                                                className="slider__nav"
                                            >
                                                {productImages.map((productImage, index) => {
                                                    return (
                                                        <div className="slider__nav__sub">
                                                            <img
                                                                key={productImage.id}
                                                                src={productImage.image_path || ''}
                                                                alt={product.product_name}
                                                            />
                                                        </div>
                                                    );
                                                })}
                                            </Slider>
                                            {/* Smaill Images End  */}
                                        </div>

                                        <div className="product__details__info">
                                            <div className="product__info">
                                                <div className="product__info__title">Thông tin sản phấm</div>
                                                <div className="product__info__content">
                                                    <p>{product.description}</p>
                                                </div>
                                            </div>

                                            <div className="product__info">
                                                <p>
                                                    Nhắn tin hoặc gọi tổng đài 1800.2044 để được hỗ trợ nếu không tìm
                                                    được cửa hàng có hàng gần bạn
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* Image End */}

                                {/* Content Start */}
                                <div className="col-lg-6">
                                    <div className="product__details__content">
                                        <div className="product__details__price">
                                            <h3>
                                                Giá sản phẩm{' '}
                                                <p>
                                                    {Intl.NumberFormat('vi-VN', {
                                                        style: 'currency',
                                                        currency: 'VND',
                                                    }).format(product.price)}
                                                </p>
                                            </h3>
                                        </div>
                                        <div className="product__details__button">
                                            <div className="product__button__group">
                                                <button onClick={handleBuy} className="btn btn--primary btn--static btn--large" to="#">
                                                    Mua ngay
                                                </button>
                                                <button
                                                    title="Thêm vào giỏ"
                                                    className="btn btn--primary btn--white  btn--large"
                                                >
                                                    <i className="fal fa-cart-plus"></i>
                                                </button>
                                            </div>
                                            <div className="product__button__group">
                                                <button
                                                    title="Thêm vào giỏ"
                                                    className="btn btn--primary btn--white  btn--large"
                                                >
                                                    Trả góp 0% <br /> Trả trước chỉ từ 0đ
                                                </button>
                                                <button
                                                    title="Thêm vào giỏ"
                                                    className="btn btn--primary btn--white  btn--large"
                                                >
                                                    Trả góp qua thẻ <br /> (VISA NAPAS mASTERCARD)
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* Content End */}
                            </div>
                        </div>

                        <div className="product__details__related">
                            <Slider slidesToShow={4} slidesToScroll={2} className="product__related__slider">
                                {/* {console.log(productRelateds)} */}

                                {productRelateds.map((product) => {
                                    return (
                                        <ProductItem
                                            key={product.id}
                                            product_id={product.id}
                                            productName={product.product_name}
                                            image={product.product_image[0]?.image_path}
                                            productPrice={product.price}
                                            className="col-lg-12"
                                        />
                                    );
                                })}
                            </Slider>
                        </div>

                        <div className=" row">
                            <div className="col-lg-8 product__details__info product__details__info--flex-col">
                                <div className="product__info__title">
                                    <h3>Đặc điểm nổi bật</h3>
                                </div>

                                <div className={full ? 'product__info__content full ' : 'product__info__content'}>
                                    {product.details}
                                </div>
                                <div className="product__info__button" onClick={handleSeefullContent}>
                                    <p>Xem thêm</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default ProductDetails;
