import { Link } from 'react-router-dom';

function ProductItem({ productName, productPrice, image, product_id, className = "col-md-6 col-lg-3 col-sm-6" }) {
  
    return (
        <div className={className}>
            <div className="product__item">
                <div className="product__item__wraps">
                    {/* Image Start */}
                    <div className="product__item__top">
                        <Link
                            to="/product-details" 
                            state= {{ product_id: product_id }} 
                            className="product__item__img"
                        >
                            <img src={image || 'https://upload.wikimedia.org/wikipedia/commons/a/ab/Apple-logo.png'} />
                        </Link>

                        <div className="product__item__functions">
                            <div className="funtion__item">
                                <i className="fal fa-shopping-bag"></i>
                            </div>

                            <div className="funtion__item">
                                <i className="fal fa-search"></i>
                            </div>

                            <div className="funtion__item">
                                <i className="fal fa-heart"></i>
                            </div>
                        </div>
                    </div>
                    {/* Image End */}

                    {/* Description Start */}
                    <div className="product__item__bottom">
                        <div className="product__item__name">
                            <h3>
                                <Link to="#" className="product__item__link">
                                    {productName}
                                </Link>
                            </h3>
                        </div>
                        <div className="product__item__price">
                            <p>
                                {Intl.NumberFormat('vi-VN', {
                                    style: 'currency',
                                    currency: 'VND',
                                }).format(productPrice)}
                            </p>
                        </div>
                    </div>
                    {/* Description End */}
                </div>
            </div>
        </div>
    );
}

export default ProductItem;
