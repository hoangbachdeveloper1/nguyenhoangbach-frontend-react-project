import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import  { masterUrl, paths } from '../../../const/baseUrl/baseUrl';
import axios from 'axios';

function HeaderCategories() {
    
    const [categories, setCategories] = useState([])

    useEffect(() => {
        axios.get(`${masterUrl}${paths.category}`)
            .then((result) => {
                setCategories(result.data) 
            })
            .catch((err) => {throw new Error(err)})
    }, []);

    return (
       
            <div className="header__midle__categories">
                <div className="categories">
                    <div className="categories__wraps">
                        <div className="categories__collapse">
                            <span>Danh mục</span>
                            <span>
                                <i className="fas fa-chevron-down"></i>
                            </span>
                        </div>
                        <div className="categories__options">
                            <ul className="categories__list">
                                {categories.map((category) => {
                                    return (
                                        <li 
                                            key={category.category_id} 
                                            className="categories__item">
                                            <Link to="/product">{category.name}</Link>
                                        </li>
                                    );
                                })}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    );
}

export default HeaderCategories;
