import { useContext } from 'react';
import HeaderProfileFeature from './headerProfileFeature';
import HeaderProfileUser from './headerProfileUser';
import LoginContext from '../../loginContext/loginContext';

function HeaderProfile() {
    // create state

    // Kiểm tra trạng thái đăng nhập khi tải trang
    // useEffect(() => {
    //     const token = JSON.parse(localStorage.getItem('token'));
    //     if (token) {
    //         setIsLoggedIn(true);
    //         axios
    //             .get(`${masterUrl}${paths.me}`, {
    //                 headers: {
    //                     Authorization: `Bearer ${token.access_token}`,
    //                 },
    //             })
    //             .then((response) => {
    //                 setUserInfo(response.data);
    //             })
    //             .catch((err) => {
    //                 throw new Error(err);
    //             });
    //     }
    // }, [isLoggedIn]);

    const {isLoggedIn, useInfo} = useContext(LoginContext)

    return (
    
            <div className="header__auth">
                <div className="header__auth__link">
                    {/* if unauthenticated, display this */}
                    {!isLoggedIn ? <HeaderProfileFeature /> : <HeaderProfileUser name={useInfo.name} />}
                    {/* if authenticated dispaly this */}
                </div>
            </div>
    );
}

export default HeaderProfile;
