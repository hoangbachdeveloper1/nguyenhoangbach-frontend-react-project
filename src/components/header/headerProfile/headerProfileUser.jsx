import DropDown from '../../dropDownComponent/dropDownComponent';
import  UserFeature from  './userFeature'
function HeaderProfileUser({name}) {

    return (
        <>
            <div className="header__auth__user">
                <div className="user">
                    <div className="user__group">
                        <div className="user__group__icon">
                            <i className="fas fa-bells"></i>   
                        </div>
                        <div className="user__group__text">
                            <span>Thông báo</span>
                        </div>
                    </div>

                    <div className="user__group">
                        <div className="user__group__icon">
                            <i className="fal fa-question-circle"></i> 
                        </div>
                        <div className="user__group__text">
                            <span>Trợ giúp</span>
                        </div>
                    </div>

                    <div className="user__group">
                    
                        <div className="user__group__image">
                            <img
                                src="https://scontent.fvii1-1.fna.fbcdn.net/v/t39.30808-1/411064760_1787589848429113_6059102228212130028_n.jpg?stp=dst-jpg_p160x160&_nc_cat=101&ccb=1-7&_nc_sid=11e7ab&_nc_ohc=c7K0GVoMSmEAX__fkjT&_nc_ht=scontent.fvii1-1.fna&oh=00_AfCX7pENxTNCLW4ZiqT-x8ECh1oDnyiSip1RyaqVW7g28Q&oe=658D1A5E"
                                title={name}
                                alt={name}
                            />
                        </div>

                        <div className="user__group__text">
                            <span>
                                {name}
                            </span>
                        </div>

                        <div className="user__dropdown">
                            <DropDown children={<UserFeature/>}/>
                        </div>

                    </div>
                </div>
            </div>
        </>
    );
}

export default HeaderProfileUser


