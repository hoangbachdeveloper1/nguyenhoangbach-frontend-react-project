import { Link } from "react-router-dom"

function HeaderProfileFeature () {
    return <>
           <ul className="header__auth__button">
                    <li className="header__auth__item">
                        <Link to='/register'>
                           <button className="btn btn-light" >
                                Đăng Kí
                           </button>
                        </Link>
                    </li>
                    <li className="header__auth__item">
                        <Link to='/login'>
                           <button className="btn btn-light">
                                Đăng nhập
                           </button>
                        </Link>
                    </li>
                </ul>
    </> 
}

export default HeaderProfileFeature