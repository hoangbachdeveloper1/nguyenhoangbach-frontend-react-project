import { Link } from "react-router-dom"
import axios from "axios";
import { masterUrl, paths } from "../../../const/baseUrl/baseUrl";
import LoginContext from "../../loginContext/loginContext";
import { useContext } from "react";

function UserFeature () {

    const {setIsLoggedIn} = useContext(LoginContext)

    const handleLogOut =(e)=> {
        const token = JSON.parse(localStorage.getItem('token'));

        e.preventDefault();
        axios.get(`${masterUrl}${paths.logout}`, {
            headers: { 'Authorization': `Bearer ${token.access_token}`,},})
            .then(response => {
                console.log(response)
                // remove token login
                localStorage.removeItem('token') 
                // change to logout state
                setIsLoggedIn(false)
            })
            .catch(err => { 

            })
    }
    return <>
        <ul className="user__dropList">
            <li className="user__dropList__item">
                <Link to={'#'}>
                    <span>
                        Tài khoản của tôi
                    </span>
                </Link>
            </li>
            <li className="user__dropList__item">
                <Link to={'/cart'}>
                    <span>
                        Đơn mua
                    </span>
                </Link>
            </li>
            <li  className="user__dropList__item">
                <Link to={'#'} onClick={handleLogOut}>
                    <span>
                        Đăng xuất
                    </span>
                </Link>
            </li>

        </ul>
    </>
}

export default UserFeature
