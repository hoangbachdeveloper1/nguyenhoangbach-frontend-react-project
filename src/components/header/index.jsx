import { logoImage } from '../../assets/images';
import HeaderLogo from './headerLogo/headerLogo';
import HeaderSearchForm from './headerSrearchForm/headerSearchForm';
import HeaderMenu from './headerBottom/headerMenu';
import { useEffect, useState } from 'react';
import HeaderProfile from './headerProfile/headerPorfile';

function HeaderContainer() {
    const [scroll, setScroll] = useState(0);
    const [fixed, setFixed] = useState(false);
    const [show, setShow] = useState(false);

    const handleSetScroll = () => {
        setScroll(window.scrollY);
    };

    window.addEventListener('scroll', handleSetScroll);

    useEffect(() => {
        if (scroll >= 200) {
            setFixed(true);
        } else {
            setFixed(false);
        }
    }, [scroll]);

    return (
        <header id="header">
            <div className="header">
                {/* NavTop Start */}
                 <div className="container">
                    <div className="header__top">
                        <div className="row header__top__wraps">
                            
                                <div className="col-lg-4">
                                    <HeaderLogo logo={logoImage.logo} />
                                </div>

                                <div className="col-lg-8">
                                    <HeaderProfile/>
                                </div>
                         
                        </div>
                    </div>
                </div> 
                {/* NavTop End */}

                

                {/* NavBottom Start */}
                <div className={fixed ? 'header__bottom active' : 'header__bottom'}>
                    <div className="container header__bottom__container">
                    <div className="row">

                        <div className="header__bottom__toggle-button">
                            <button onClick={()=>setShow(!show)}>
                                <i className="far fa-bars"></i>
                            </button>
                        </div>

                            <HeaderMenu show={show}  />
                            <HeaderSearchForm />
                            
                        </div>
                    </div>
                </div>
                {/* NavBottom End */}
            </div>
        </header>
    );
}

export default HeaderContainer;
