// ExampleContext.js
import { createContext } from 'react';

const HeaderLoginContext = createContext();

export default HeaderLoginContext;