import { useState, useCallback, useRef } from 'react';
import axios from 'axios';
import {masterUrl, paths } from '../../../const/baseUrl/baseUrl';
import { debounce } from 'lodash';
import { Link } from 'react-router-dom';

function HeaderSearchForm() {

    const [search, setSearch] = useState('');
    const [searchResponse, setSearchResponse] = useState([])
    const [dropDownActive, setDropDownActive] = useState(false)
    const headerSearchListRef = useRef();

    const handleOnChangeSearch = (e) => {
        setSearch(e.target.value);
        // if value of search box not empty then call query method
        if (e.target.value.length) {
            debounceQuery(e.target.value);
        }
    };

    const handleSubmitSearchForm = (e) => {
        e.preventDefault();
    };

    // create an debouce function
    const debounceQuery = useCallback(
        debounce((nextValue) => handleQuerySeach(nextValue), 1000),
        [],
    );
    
    const handleQuerySeach = (query) => {
        axios.post(`${masterUrl}${paths.search}`, { query }).then((res) => {
            const response = res.data.map((data) =>(
                {
                   
                    ...data.searchable,
                    type : data.type,
                    title : data.title
                }
            ))
            // an item of res.data have a form belike: {searchable : {properties}}

            setSearchResponse([...response]);
            setDropDownActive(true)
        });
        // call function axios when forrm submitting
        // method post and transmit the query variable to the #2 arguments
    };
  
    const handleOnBlurSearchInput =() => {
        window.addEventListener('click', e=> {
            if(!headerSearchListRef.current.contains(e.target) ) {
                setDropDownActive(false)
                setSearchResponse([])
            }
        })
    
    }

    return (
        
        <div className="col-lg-5  header__search__wraps ">
            <div className="header__search">
                <div className="header__search__form">
                    <form action="#" method="POST" onSubmit={handleSubmitSearchForm}>
                        <input
                            type="text"
                            value={search}
                            name="search"
                            onChange={handleOnChangeSearch}
                            onBlur={handleOnBlurSearchInput}
                            placeholder="Gõ từ khóa bạn muốn tìm..."
                        />
                        <button type="submit">
                            <i className="far fa-search"></i>
                        </button>
                    </form>

                    <ul ref={headerSearchListRef} className={`header__search__list ${dropDownActive ? 'active' : ''}`}>
                        {searchResponse.map((item, index) => (
                            <li key={index}>
                                <Link to={`/${item.type}`}>
                                    <b>
                                        {item.title}
                                    </b>
                                    <span>
                                        {item.type}
                                    </span>
                                </Link>
                            </li>
                        ))}
                    </ul>

                </div>
            </div>
        </div>
    );
}

export default HeaderSearchForm;
