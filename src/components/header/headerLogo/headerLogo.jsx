import { Link } from "react-router-dom";

function HeaderLogo({logo}) {
    return (
    <div className="col-md-3 col-lg-3 col-sm-3 no-padding header__logo__wraps">
        <div className="header__midle__logo">
            <Link to="/">
                <img src={logo} alt="HoangBachNhaKhoa" />
            </Link>
        </div>
    </div>
    );
}

export default HeaderLogo;