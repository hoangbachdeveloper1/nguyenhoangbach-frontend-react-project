import { Link } from 'react-router-dom';
import navList from '../../../const/navList/navList';
import { useRef, useState } from 'react';
import HeaderSubMenu from './HeaderSubMenu';

function HeaderMenu({ show }) {

    const [active, setActive] = useState(1);

    const headerBottomMenuRef = useRef();

    const handleSetActive = (id) => {
        setActive(id);
    };

    // Hide menu in responsive
    const handleSetHide = (e) => {
    //   console.log(e.target.className)
    };

    window.addEventListener('click', handleSetHide);

    return (
        <div className="col-lg-7">
            <div ref={headerBottomMenuRef}  className={show ? 'header__bottom__menu active' : 'header__bottom__menu'}>
                <nav>
                    <ul>
                        {navList.map((item) => {
                            return (
                                <li
                                    key={item.id}
                                    className={
                                        active === item.id ? 'header__bottom__wraps  active' : 'header__bottom__wraps '
                                    }
                                    onClick={(e) => {
                                        handleSetActive(item.id);
                                    }}
                                >
                                    <div className="header__bottom__item">
                                        <Link
                                            to={item.link}
                                            className={
                                                item.sub
                                                    ? 'item__dropn-down header__bottom__link'
                                                    : 'header__bottom__link'
                                            }
                                        >
                                            <span>{item.text}</span>
                                        </Link>
                                    </div>
                                    {item.sub ? <HeaderSubMenu /> : ''}
                                </li>
                            );
                        })}
                    </ul>
                </nav>
            </div>
        </div>
    );
}

export default HeaderMenu;
