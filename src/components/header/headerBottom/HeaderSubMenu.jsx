import { Link } from "react-router-dom";                                                             
import { useEffect, useState } from "react";
import axios from "axios";
import { masterUrl, paths } from "../../../const/baseUrl/baseUrl";
function HeaderSubMenu() {


    const [categories, setCategories] = useState([])

    useEffect (()=> {
        axios.get(`${masterUrl}${paths.category}`)  
            .then(response=> setCategories(response.data))
            .catch(err => {throw new Error(err)})
        return ()=> {

            setCategories([])
        }
    }, [])


    return ( <div className="header__sub">
                    <div className="header__sub__inner">
                        <ul className="header__sub__list">
                            {
                                categories.map(item =>  (
                                    <li className="header__sub__item" key={item.category_id}>
                                        <div className="sub__item">
                                            <Link to={item.link}>
                                                <span>
                                                    { item.name }
                                                </span>
                                            </Link>
                                        </div>
                                    </li>
                                ))                            }
                        </ul>
                    </div>
    </div> );
}

export default HeaderSubMenu;