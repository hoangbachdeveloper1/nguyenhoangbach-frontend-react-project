import { Routes,  Route } from "react-router-dom";

import LandingPage from "../body/landingPage/landingPage";
import Aboutpage from "../about/about";
import NewsPage from "../news/news";
import CreateNews from "../news/createNews";
import NewsPostpage from "../news/newsPost";
import NewsList from "../news/newsList";
import HirringPage from "../hirring/hirring";
import ContactPage from "../contact/contact";
import ProductPage from "../products/product";
import CartPage from "../cart/cart";
import CheckoutPage from "../checkout/checkout";
import LoginPage from "../authentical/login";
import RegisterPage from "../authentical/register";
import CreateProduct from "../products/createProduct";
import ProductDetails from "../products/productDetails";



function RoutesComponents() {
    return (  <Routes> 
        <Route path="/" element={<LandingPage/>} />
        <Route path="/about" element={<Aboutpage/>} />
        <Route path="/news" element={<NewsPage/>} />
        <Route path="/news/news-post" element={<NewsPostpage/>} />
        <Route path="/news/news-create" element={<CreateNews/>} />
        <Route path="/news/news-list" element={<NewsList/>} />
        <Route path="/hirring" element={<HirringPage/>} />
        <Route path="/contact" element={<ContactPage/>}/>
        <Route path="/products" element={<ProductPage/>} />
        <Route path="/cart" element={<CartPage/>} />
        <Route path="/checkout" element={<CheckoutPage/>} />
        <Route path="/login" element={<LoginPage/>} />
        <Route path="/register" element={<RegisterPage/>} />
        <Route path="/create-product" element={<CreateProduct/>} />
        <Route path="/product-details" element={<ProductDetails/>} />
        </Routes> );
}

export default RoutesComponents;
