import Breadcrumb from '../breadcrumb/breadcrumb';
import { aboutListImg } from '../../assets/images';
import { storyParagraph, storyServices } from '../../const/about/about';
import { aboutPartnerImg, aboutActivities } from '../../assets/images';

function Aboutpage() {
    return (
        <div className="about">
            <Breadcrumb title={'Giới thiệu'} link={'/about'} />

            <div className="about__content">
                {/* Content start */}
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 col-md-6">
                            <div className="about__story">
                                {aboutListImg.map((img, index) => (
                                    <div key={index} className="about__story__img">
                                        <div className="story__img__item">
                                            <img src={img} alt="about-img" />
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-6">
                            <div className="about__story">
                                <div className="story__content">
                                    <h2>CÂU CHUYỆN VỀ LOGITECH</h2>
                                    <div className="story__content">
                                        {storyParagraph.map((item,  index) => (
                                            <p key={index} >{item.text}</p>
                                        ))}
                                    </div>

                                    <div className="story__services">
                                        {storyServices.map((item, index) => (
                                            <div key={index} className="story__services__item">
                                                <i className="fas fa-play"></i>
                                                <p>{item}</p>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* COntent End */}

                {/* Partner Start */}
                <div className="about__partner">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="about__partner__left">
                                    <div className="about__title"><h2>
                                    ĐỐI TÁC CỦA CHÚNG TÔI
                                    </h2></div>

                                    <div className="about__partner__paragraph">
                                        <p>
                                            Chúng tôi cố gắng mỗi ngày, từng bước một, phục vụ quý khách một cách tốt
                                            nhất. Chúng tôi vui mừng vì phần lớn khách hàng luôn muốn quay lại hợp tác
                                            với FGC không chỉ lần đâu tiền. Điều đó chứng tỏ chúng tôi đang nỗ lực đúng
                                            hướng nhằm thỏa mãn nhu cầu ngày càng cao của khách hàng. Nếu còn có điều gì
                                            chúng tôi chưa làm Quý vị hài lòng, xin vui lòng báo để chúng tôi được biết
                                            và tiếp thu.
                                        </p>

                                        <p>
                                            Hãy trải nghiệm hợp tác với FGC, Quý vị sẽ cảm thấy hài lòng với sự đầu tư
                                            đúng đắn và hợp lý của mình.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="about__partner__right">
                                    <img src={aboutPartnerImg} alt='about__partner__right' />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Partner End */}

                <div className="about__activities">
                        <div className="about__title">
                            <h2>
                                Hoạt động của chúng tôi
                            </h2>
                        </div>
                        <div className="container">
                            <div className="row">
                            {
                                aboutActivities.map((item, index) => {
                                    return  <div key={index} className="col-md-4">
                                     <div className="about__activities__img">
                                        <img src={item} alt='activities__img'/>
                                     </div>
                                </div>
                                })
                            }
                               
                            </div>
                        </div>
                </div>
            </div>
        </div>
    );
}

export default Aboutpage;
