import { Link } from 'react-router-dom';
import { logoImage } from '../../assets/images';
import { footerCategories, privacy } from '../../const/categories/categories';
import { useEffect, useState } from 'react';
import { masterUrl, paths } from '../../const/baseUrl/baseUrl';
import axios from 'axios';

function FooterMid() {


    const [brands, setBrands] = useState([])
    const [categories, setCategories]   = useState([])

    useEffect(()=> {

        axios.get(`${masterUrl}${paths.brandsImage}`)
            .then(response=>setBrands(response.data))
            .catch()
        axios.get(`${masterUrl}${paths.category}`)
            .then(response=>setCategories(response.data))
            .catch()
        return ()=> {
            setBrands([])
            setCategories([])
        }
    }, [])

    return (
        <div className="footer__mid">
            <div className="footer__mid__container container">
                <div className="row">
                    <div className=" col-md-12 col-lg-3">
                        <div className="footer__mid__logo">
                            <Link to="/">
                                <img src={logoImage.logo} alt="logo" />
                            </Link>
                        </div>

                        <div className="footer__mid__text">
                            <div className="text__group">
                                <b>Địa chỉ:</b>{' '}
                                <span>
                                    {' '}
                                    Thành phố Vinh - Nghệ An 
                                </span>
                            </div>
                            <div className="text__group">
                                <b>Email:</b> <span> hoangbachdeveloper@gmail.com</span>
                            </div>
                            <div className="text__group">
                                <b>Điện thọai:</b> <span className="text__group__phone">0373 - 062 - 099</span>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6 col-lg-6 col-sm-12">
                        <div className="footer__mid__categories">
                            <div className="mid__title">
                                <h3>ĐỐI TÁC CỦA CHÚNG TÔI</h3>
                            </div>

                            <div className="mid__categories__list">
                                <ul>
                                    {brands.map((brand) => (
                                        <li className="mid__categories__item" key={brand.brand_id}>
                                            <Link to="#">
                                                <i className="fas fa-caret-right"></i>
                                                <p>{brand.brand_name}</p>
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6 col-lg-3 col-sm-12">
                        <div className="footer__mid__categories">

                          <div className="mid__title">
                                <h3>DANH MỤC SẢN PHẨM</h3>
                            </div>

                            <div className="mid__categories__list">
                                <ul>
                                    {categories.limit(5).map((category) => (
                                        <li className="mid__categories__item" key={category.category_id}>
                                            <Link to="#">
                                                <i className="fas fa-caret-right"></i>
                                                <p>{category.name}</p>
                                            </Link>
                                        </li>
                                    ))}
                                </ul>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FooterMid;
