import { useState } from 'react';
import { socialIcons } from '../../const/socialsIcon/socialIcons';

function FooterTop() {

    const [email, setEmail] = useState('')

    return (
        <div className="footer__top">
            <div className="footer__top__container container">
                <div className="row">
                    {/* Social Icons Start */}
                    <div className="col-lg-6 col-md-6">
                        <div className="footer__top__subscribe">
                            <div className="subscribe__title">
                                <h2>Theo dõi:</h2>
                            </div>

                            <div className="subscribe__list">
                                <ul>
                                    {socialIcons.map((icon) => (
                                        <li
                                        key={icon.id}
                                            title={icon.title}
                                        >
                                            <a className={'subscribe__link subscribe__link--' + icon.className} href={icon.link}>
                                                {icon.IconName}
                                            </a>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>
                    {/* Social Icons End */}

                    {/* Subscribe Form Start */}
                    <div className="col-lg-6 col-md-6">
                        <div className="footer__top__form">
                        
                            <div className="form__content">
                                <form action="#" method="post">
                                    <div className="form-group form__content__input">
                                        <input
                                            className="form-control"
                                            placeholder="Đăng kí để nhận khuyến mãi..."
                                            value={email}
                                            onChange={(e)=> {setEmail(e.target.value)}}
                                        />
                                    </div>

                                    <div className="form__content__submit" >
                                        <button type="submit" >
                                            Đăng ký
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {/* Subscribe Form End */}

                </div>
            </div>
        </div>
    );
}

export default FooterTop;
