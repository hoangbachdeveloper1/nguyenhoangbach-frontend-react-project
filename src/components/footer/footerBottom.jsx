import { paymentImg } from '../../assets/images' 

function FooterBottom() {
    return (
        <div className="footer__bottom">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 col-md-6 ">
                        <div className="footer__bottom__left">
                            <p>
                                <i className="far fa-copyright"></i>
                                <span>
                                nguyenhoangbach - phát triển bởi Nguyễn Hoàng Bách
                                </span>
                            </p>
                        </div>
                    </div>

                    <div className="col-lg-6 col-md-6">
                        <div className="footer__bottom__right">
                            <img src={paymentImg} alt="paymen-img"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FooterBottom;
