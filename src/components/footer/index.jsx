import FooterTop from "./footerTop";
import FooterMid from "./footerMid";
import FooterBottom from "./footerBottom";

function FooterComponent() {
    return ( <footer>
        <div className="footer">
            <div className="footer__container">
                <FooterTop/>
                <FooterMid/>
                <FooterBottom/>
            </div>
        </div>
    </footer> );
}

export default FooterComponent;