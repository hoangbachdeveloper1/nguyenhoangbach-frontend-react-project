import { useRef, useState } from 'react';

function CheckoutForm() {
    const [create, setCreate] = useState(false);
    const formRef = useRef()

    const handleCreate = () => {
        setCreate(!create);
    };

    const handleSubmit=()=> {
        
    }

    return (
        <div className="checkout__form">
            <form ref={formRef} id="guess-form" action="#" method="get">
                <div className="checkout__form-groupd form__group--custom">
                    <input
                        type="email"
                        name="guess-email"
                        className="checkout__form-control form__control--custom"
                        placeholder="Email"
                        required
                    />
                </div>

                <div className=" form__group--double form__group--custom">
                    <input
                        type="text"
                        name="guess-first-name"
                        className="checkout__form-control checkout__form-control--modify form__control--custom"
                        placeholder="Họ"
                        required
                    />

                    <input
                        type="text"
                        name="guess-last-name"
                        className="checkout__form-control checkout__form-control--modify form__control--custom"
                        placeholder="Tên"
                        required
                    />
                </div>

                <div className="form__group--custom">
                    <div className="form__group--flex">
                        <input hidden type="checkbox" name="guess-create-pass" id="guess-pass" />
                        <label
                            onClick={handleCreate}
                            htmlFor="guess-pass"
                            className={`${create ? 'active' : ''} form__label form__label--box`}
                        >
                            <i className="far fa-check"></i>
                        </label>
                        <label htmlFor="guess-pass" className="form__label form__label--text" onClick={handleCreate}>
                            Tạo tài khoản mới
                        </label>
                    </div>
                </div>

                <div className={`${create ? 'active' : ''} form__group--hiddend`}>
                    {create ? (
                        <div className="form__group--custom">
                            <div className="form__group--double">
                                <input
                                    type="password"
                                    name="guess-pass"
                                    className="checkout__form-control checkout__form-control--modify form__control--custom checkout__form-control--required"
                                    placeholder="Mật khẩu (Tùy chọn)"
                                    required
                                />
                                <input
                                    type="password"
                                    name="guess-confirm"
                                    className="checkout__form-control checkout__form-control--modify form__control--custom checkout__form-control--required"
                                    placeholder="Xác nhận mật khẩu"
                                    required
                                />
                            </div>
                        </div>
                    ) : (
                        ''
                    )}
                </div>

                <div className="checkout__form-groupd form__group--custom">
                    <button onClick={handleSubmit}  type="submit" className="btn btn--primary btn--large">
                        Tiếp tục
                    </button>
                </div>
            </form>
        </div>
    );
}

export default CheckoutForm;
