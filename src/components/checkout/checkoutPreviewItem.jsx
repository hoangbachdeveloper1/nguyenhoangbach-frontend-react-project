function CheckoutPreviewItem({ product }) {
    return (
        <li>
            <div class="checkout__cart__item">
                <div class="checkout__cart__wraps">
                    <a href="product-details.html" class="checkout__cart__img">
                        <img src={product.image} alt="Thiết bị nha khoa 1" />
                        <span>1</span>
                    </a>
                    <div class="checkout__cart__info">
                        <a class="checkout__cart__name" href="product-detail.html">
                            {product.name}
                        </a>
                        <span class="checkout__cart__description">Màu sắc: Trắng, Chất liệu: Kim loại</span>
                    </div>
                </div>
                <div class="checkout__cart__price">
                    <span>40.000VND</span>
                </div>
            </div>
        </li>
    );
}

export default CheckoutPreviewItem;
