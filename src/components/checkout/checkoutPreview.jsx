import { productList } from '../../const/products/products';
import CheckoutPreviewItem from './checkoutPreviewItem';

function CheckoutPreview() {
    return (
        <div class="checkout__cart mb-5">
            <div class="checkout__cart__inner">
                <div class="checkout__cart__list">
                    <ul>
                        {productList.map((product) => (
                            <CheckoutPreviewItem key={product.id} product={product} />
                        ))}
                    </ul>
                </div>
                <div class="checkout__cart__table">
                    <table>
                        <tbody>
                            <tr>
                                <td class="t-left">Tạm tính</td>
                                <td class="t-right">160.000.000VNĐ</td>
                            </tr>
                            <tr>
                                <td class="t-left">Phí vận chuyển</td>
                                <td class="t-right">Miễn phí</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="checkout__cart__total">
                    <span>Tổng tiền</span>
                    <p>160.000.000VNĐ</p>
                </div>
            </div>
        </div>
    );
}

export default CheckoutPreview;
