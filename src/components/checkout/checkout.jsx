import { Link } from 'react-router-dom';
import Breadcrumb from '../breadcrumb/breadcrumb';
import CheckoutForm from './checkoutForm';
import CheckoutPreview from './checkoutPreview';

function CheckoutPage() {
    return (
        <div className="checkout">
            <Breadcrumb title={'Thanh toán giỏ hàng của bạn'} />
            <div className="checkout__container container ">
                <div className="row">
                    <div className="col-md-7">
                    <div className="checkout__title">
                        <h3>
                            Thông tin cá nhân
                        </h3>

                        <p>
                            <span>
                                Bạn đã có tài khoản? 
                            </span>
                            <Link to={"/login"}> 
                                Đăng nhập
                            </Link>
                        </p>
                    </div>
                        <CheckoutForm/>
                    </div>
                    <div className="col-md-5">
                        <CheckoutPreview/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CheckoutPage;
