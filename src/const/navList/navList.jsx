const navList = [
    {
        id: 1,
        text: "Trang chủ", 
        link: '/',
        sub: false
    }, 
    {
        id: 2,
        text: "Giới thiệu", 
        link: '/about',
        sub: false
    }, 
    {
        id: 3,
        text: "Sản phẩm", 
        link: '/products',
        sub: true
    },
    {
        id: 5,
        text: "Tin tức",
        link: '/news',
        sub: false
    }, 
    {
        id: 6,
        text: "Tuyển dụng", 
        link: '/hirring',
        sub: false
    }, 
    {
        id: 7,
        text: "Liên hệ", 
        link: '/contact',
        sub: false
    }
]

export default navList