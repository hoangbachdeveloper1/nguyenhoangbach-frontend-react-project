const storyParagraph = [
    {
        id: 1,
        text: 'Công ty Cổ phần Giải pháp công nghệ FGC (FGC Techlution) là Công ty hoạt động trong lĩnh vực tư vấn và cung cấp các giải pháp về hệ thống thông tin, quản trị dữ liệu và thương mại điện tử; Tư vấn, thiết kế website cho các tổ chức, doanh nghiệp và cá nhân trong và ngoài nước.',
    },
    {
        id: 2,
        text: 'Với đội ngũ nhân viên trẻ trung năng động và nhiệt tình, chuyên nghiệp cùng với sự ủng hộ tinh thần khởi nghiệp (startup) của những người trẻ, FGC Techlution đang xây dựng một môi trường để đón nhận các ý tưởng công nghệ mới và kết nối mọi người để thực hiện chúng.'
    },
];

const storyServices = [
        "Đặt lợi ích khách hàng lên trên hết",
        "Tất cả chúng ta đều mong muốn cống hiến sức lao động của mình",
        "Sáng tạo là suy nghĩ 1 cách hiệu quả hơn",
        "Cùng chia sẻ ý tưởng, cùng kết nối ước mơ",
        "Lòng tin vào một tương lai bền vững tạo sức mạnh sáng tạo"
]

export {
    storyParagraph, 
    storyServices
}
