export const socialIcons = [
    {
        id: 0, 
        className: "facebook",
        IconName: <i className="fab fa-facebook-f"></i>,
        title: "Facebook",
        link: "https://www.facebook.com/nguyenhoangminhbach"
    },
    {
        id: 1, 
        className: "google",
        IconName: <i className="fab fa-google"></i>,
        title: "Google", 
        link: "https://myaccount.google.com/u/1/?hl=vi&utm_source=OGB&utm_medium=act"
    },
    {
        id: 2, 
        className: "twitter",
        IconName: <i className="fab fa-twitter"></i>,
        title: "Twitter", 
        link: "https://twitter.com/?lang=vi"
    },
    {
        id: 3, 
        className: "instagram",
        IconName: <i className="fab fa-instagram"></i>,
        title: "Instagram",
        link: "https://www.instagram.com/"
    }
]