export const services = [
    {
        id: 0, 
        title: "GIAO HÀNG MIỄN PHÍ",
        description: "Miễn phí toàn quốc",
        image: require('../../assets/images/icons/services/001-truck.png')
    },
    {
        id: 1, 
        title: "BẢO HÀNH TRỌN ĐỜI",
        description: "Chất lượng quốc tế",
        image: require('../../assets/images/icons/services/003-frame.png')
    },
    {
        id: 2, 
        title: "TƯ VẤN 24/7",
        description: "Chuyên nghiệp",
        image: require('../../assets/images/icons/services/002-customer-service.png')
    },
    {
        id: 3, 
        title: "DỊCH VỤ THỨ 4",
        description: "Chuyên nghiệp",
        image: require('../../assets/images/icons/services/001-truck.png')
    }
]