
const masterUrl = 'http://127.0.0.1:8000/api';
const paths = {
    category: '/categories',
    brand: '/brands',
    product: '/products',
    productImage: '/product-images',
    productRelatedBrand: "/product-related-brand",
    search: '/search',
    bill: '/bills',
    categoryOutstanding: '/categories-outstanding',
    newProduct: '/products-new',
    services: '/services',
    customerEvaluate: '/user-evaluate',
    news: '/posts',
    newList: '/post-list',
    brandsImage: '/brands-image', 
    contact: '/contact',
    register: '/register',
    login: '/login',
    logout: '/logout',
    user: '/users',
    me: '/me'
};

export { masterUrl, paths };
