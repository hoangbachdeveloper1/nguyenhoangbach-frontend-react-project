export const categories = [
    {
        id: 0,
        name: "Nội thất phòng khám",
    },
    {
        id: 1,
        name: "Máy nén khí",
    },
    {
        id: 2,
        name: "Thiết bị vệ sinh",
    },
    {
        id:3,
        name: "Chẩn đoán hình ảnh",
    },
    {
        id: 4,
        name: "Máy cạo vôi",
    },
    {
        id: 5,
        name: "Tay khoan",
    },
    {
        id: 6,
        name: "Ghế máy nha khoa",
    },
    {
        id: 7,
        name: "Thiết bị tiệt trùng",
    },
    {
        id: 8,
        name: "Sản phẩm nổi bật",
    },
    {
        id: 9,
        name: "Sản phẩm khuyến mãi",
    },
]

export const landingCategories = [
    {
        id: 0, 
        title: "Niềng răng trong suốt",
        img_path: require('../../assets/images/slide/banner5.jpg')
    },
    {
        id: 1, 
        title: "Dụng cụ nhổ răng",
        img_path: require('../../assets/images/slide/banner6.jpg')
    }
]

export const footerCategories = [
    {
        id: 0,
        name: "Ghế nội nha",
    },
    {
        id: 1,
        name: "Tay khoan nha khoa",
    },
    {
        id: 2,
        name: "Thiết bị chụp X-quang",
    },
    {
        id: 3,
        name: "Nha koa phục hồi",
    },
    {
        id: 4,
        name: "Đèn trám led",
    },
    {
        id: 5,
        name: "Thiết bị nội nha",
    },
    {
        id: 6,
        name: "Nha chu",
    },
    {
        id: 7,
        name: "Vệ sinh vô trùng",
    },
    {
        id: 8,
        name: "Thiết bị labo",
    },
    {
        id: 9,
        name: "Chỉnh nha",
    },
]

export const privacy = [
    {
        id: 0,
        name: "Tìm kiếm",
    },
    {
        id: 1,
        name: "Liên hệ",
    },
    {
        id: 2,
        name: "Bảo hành",
    },
    {
        id: 3,
        name: "Bảo mật khách hàng",
    },

]