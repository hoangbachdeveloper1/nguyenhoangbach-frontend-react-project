export const collects = [
    {
        id: 0, 
        idText: 'newProduct',
        name: "Hàng mới",
        link: '/products-new',
        active: true
    },
    {
        id: 1, 
        idText: 'bestSeller',
        name: "Bán chạy nhất",
        link: '/products-bestSeller',

        active: false
    },
    {
        id: 2, 
        idText: 'promotions',
        name: "Khuyến mãi",
        link: '/products-promotion',

        active: false
    }
]