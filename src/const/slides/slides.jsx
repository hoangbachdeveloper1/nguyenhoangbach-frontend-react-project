export const slideList = [
    {
        img : require('../../assets/images/slide/banner1.png'),
        text: "Phòng khám hiện đại theo tiêu chuẩn châu ÂU ",
        buttonText: "Đăng kí ngay"
    },
    {
        img : require('../../assets/images/slide/banner2.jpg'),
        text: "Thiết bị đa khoa hiện đại bậc nhất thế giới",
        buttonText: "Mua ngay"
    },
    {
        img : require('../../assets/images/slide/banner3.png'),
        text: "Phòng khám hiện đại theo tiêu chuẩn châu ÂU",
        buttonText: "Mua ngay"
    }
]