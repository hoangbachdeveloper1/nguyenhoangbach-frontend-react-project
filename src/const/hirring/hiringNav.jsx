export const hirringNavs = [
    {
        id: "HeadOfTechnicalDepartment", 
        title: "Trưởng phòng kĩ thuật",
        selected: true
    },
    {
        id: "accountant", 
        title: "Nhân viên kế toán tổng hợp",
        selected: false
    },
    {
        id: "business", 
        title: "Nhân viên kinh doanh",
        selected: false
    },
    {
        id: "marketing", 
        title: "Nhân viên Marketing - Đồ họa",
        selected: false
    },
    {
        id: "it", 
        title: "Nhân viên IT",
        selected: false
    }
]