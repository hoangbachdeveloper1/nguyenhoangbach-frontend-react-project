export const newsList = [
    {
        id: 0,
        title: 'Sẽ có một làn sóng Corona Virus thứ 2',
        desc: 'Trước làn sóng COVID-19 một lần nữa đổ bộ vào Việt Nam, kéo theo đó là nền kinh tế đang bị giảm sút, chính vì điều này khiến những khách hàng đang mong muốn',
        image_path: require('../../assets/images/blogs/1.webp')
    },
    {
        id: 1,
        title: 'Thói quen cho răng miệng chắc khỏe',
        desc: 'Trước làn sóng COVID-19 một lần nữa đổ bộ vào Việt Nam, kéo theo đó là nền kinh tế đang bị giảm sút, chính vì điều này khiến những khách hàng đang mong muốn cải thiện hàm răng của mình không khỏi phải đắn đo,',
        image_path: require('../../assets/images/blogs/2.webp')
    },
    {
        id: 2,
        title: 'Làm sao để giảm bớt căng thẳng',
        desc: 'Trước làn sóng COVID-19 một lần nữa đổ bộ vào Việt Nam, kéo theo đó là nền kinh tế đang bị giảm sút, chính vì điều này khiến những khách hàng đang mong muốn cải thiện hàm răng của mình không khỏi phải đắn đo,',
        image_path: require('../../assets/images/blogs/3.webp')
    },
    {
        id: 3,
        title: 'Sẽ có một làn sóng Corona Virus thứ 2',
        desc: 'Trước làn sóng COVID-19 một lần nữa đổ bộ vào Việt Nam, kéo theo đó là nền kinh tế đang bị giảm sút, chính vì điều này khiến những khách hàng đang mong muốn',
        image_path: require('../../assets/images/blogs/1.webp')
    },
    {
        id: 4,
        title: 'Thói quen cho răng miệng chắc khỏe',
        desc: 'Trước làn sóng COVID-19 một lần nữa đổ bộ vào Việt Nam, kéo theo đó là nền kinh tế đang bị giảm sút, chính vì điều này khiến những khách hàng đang mong muốn cải thiện hàm răng của mình không khỏi phải đắn đo,',
        image_path: require('../../assets/images/blogs/2.webp')
    },
    {
        id: 5,
        title: 'Làm sao để giảm bớt căng thẳng',
        desc: 'Trước làn sóng COVID-19 một lần nữa đổ bộ vào Việt Nam, kéo theo đó là nền kinh tế đang bị giảm sút, chính vì điều này khiến những khách hàng đang mong muốn cải thiện hàm răng của mình không khỏi phải đắn đo,',
        image_path: require('../../assets/images/blogs/3.webp')
    },
    {
        id: 6,
        title: 'Sẽ có một làn sóng Corona Virus thứ 2',
        desc: 'Trước làn sóng COVID-19 một lần nữa đổ bộ vào Việt Nam, kéo theo đó là nền kinh tế đang bị giảm sút, chính vì điều này khiến những khách hàng đang mong muốn',
        image_path: require('../../assets/images/blogs/1.webp')
    },
    {
        id: 7,
        title: 'Thói quen cho răng miệng chắc khỏe',
        desc: 'Trước làn sóng COVID-19 một lần nữa đổ bộ vào Việt Nam, kéo theo đó là nền kinh tế đang bị giảm sút, chính vì điều này khiến những khách hàng đang mong muốn cải thiện hàm răng của mình không khỏi phải đắn đo,',
        image_path: require('../../assets/images/blogs/2.webp')
    },
    {
        id: 8,
        title: 'Làm sao để giảm bớt căng thẳng',
        desc: 'Trước làn sóng COVID-19 một lần nữa đổ bộ vào Việt Nam, kéo theo đó là nền kinh tế đang bị giảm sút, chính vì điều này khiến những khách hàng đang mong muốn cải thiện hàm răng của mình không khỏi phải đắn đo,',
        image_path: require('../../assets/images/blogs/3.webp')
    }
];
