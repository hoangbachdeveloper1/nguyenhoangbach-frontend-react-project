export const newsRelateItems = [
    {
        id: 0,
        title: 'Dinh dưỡng hạn chế sâu răng',
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        img: require('../../assets/images/blogs/blog1.jpg')
    },
    {
        id: 1,
        title: 'Dinh dưỡng hạn chế sâu răng',
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        img: require('../../assets/images/blogs/blog2.jpg')
    },
    {
        id: 2,
        title: 'Dinh dưỡng hạn chế sâu răng',
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        img: require('../../assets/images/blogs/blog3.jpg')
    },
    {
        id: 3,
        title: 'Dinh dưỡng hạn chế sâu răng',
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        img: require('../../assets/images/blogs/blog4.png')
    },
    {
        id: 4,
        title: 'Dinh dưỡng hạn chế sâu răng',
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        img: require('../../assets/images/blogs/blog5.png')
    },
    {
        id: 5,
        title: 'Dinh dưỡng hạn chế sâu răng',
        description:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        img: require('../../assets/images/blogs/blog6.jpg')
    },
];
