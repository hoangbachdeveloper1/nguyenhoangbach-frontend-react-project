export const    productList = [
    {
        id: 0,
        name: "Sản phẩm nha khoa 1",
        price: "170.000",
        image: require('../../assets/images/products/1.jpg')
    },
    {
        id: 1,
        name: "Sản phẩm nha khoa 2",
        price: "270.000",
        image: require('../../assets/images/products/2.jpg')
    },
    {
        id: 2,
        name: "Sản phẩm nha khoa 3",
        price: "150.000",
        image: require('../../assets/images/products/3.jpg')
    },
    {
        id: 3,
        name: "Sản phẩm nha khoa 4",
        price: "170.000",
        image: require('../../assets/images/products/4.jpg')
    },
    {
        id: 4,
        name: "Sản phẩm nha khoa 5",
        price: "180.000",
        image: require('../../assets/images/products/1.jpg')
    },
    {
        id: 5,
        name: "Sản phẩm nha khoa 6",
        price: "270.000",
        image: require('../../assets/images/products/2.jpg')
    },
    {
        id: 6,
        name: "Sản phẩm nha khoa 7",
        price: "170.000",
        image: require('../../assets/images/products/4.jpg')
    },
    {
        id: 7,
        name: "Sản phẩm nha khoa 8",
        price: "15560.000",
        image: require('../../assets/images/products/3.jpg')
    },
    {
        id: 8,
        name: "Sản phẩm nha khoa 9",
        price: "560.000",
        image: require('../../assets/images/products/4.jpg')
    },
    {
        id: 9,
        name: "Sản phẩm nha khoa 10",
        price: "179.000",
        image: require('../../assets/images/products/1.jpg')
    },
    {
        id: 10,
        name: "Sản phẩm nha khoa 11",
        price: "270.000",
        image: require('../../assets/images/products/2.jpg')
    },
    {
        id: 11,
        name: "Sản phẩm nha khoa 12",
        price: "130.000",
        image: require('../../assets/images/products/3.jpg')
    }
]