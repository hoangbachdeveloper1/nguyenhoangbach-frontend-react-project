import axios from 'axios';
import { masterUrl, paths } from '../const/baseUrl/baseUrl';

function CreateLimit () {
    function limit(c){
        return this.filter((x,i)=>{
            if(i<=(c-1)){return true}
        })
    }

        
    Array.prototype.limit = limit;
}

function CallApi(api) {
    if(typeof api === 'string') {
        return fetch(api)
         .then(response=> response.json())
         .catch(error=> {throw new Error(error)})
     }
}

function GetUserId () {
    
    const token = JSON.parse(localStorage.getItem('token'));
    
    if(token) {
   
      return axios.get(`${masterUrl}${paths.me}`, {
        headers: {
          'Authorization': `Bearer ${token.access_token}`,
      },
      })
      .then(response => {
        return new Promise((resolve, reject) => {
                              resolve(response.data)
                            })
                          })
      .catch()
    }
}



export {CreateLimit, CallApi, GetUserId }