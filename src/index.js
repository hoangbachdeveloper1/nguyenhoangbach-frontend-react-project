import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import { CreateLimit, CallApi } from './functions/functions';



const root = ReactDOM.createRoot(document.getElementById('root'));


// Reagion create function
CreateLimit()
CallApi()
// End Region

root.render(  <BrowserRouter> <App />  </BrowserRouter>,);
reportWebVitals();
