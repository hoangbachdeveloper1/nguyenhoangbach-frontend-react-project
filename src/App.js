import './scss/style.scss'
import './App.css';
import { useState, useEffect } from 'react';
import HeaderContainer from './components/header';
import BodyComponent from './components/body';
import FooterComponent from './components/footer/';
import axios from 'axios';
import { masterUrl, paths } from './const/baseUrl/baseUrl';
import LoginContext from './components/loginContext/loginContext';

function App() {

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [useInfo, setUserInfo] = useState({}) 
  
   // Kiểm tra trạng thái đăng nhập khi tải trang
   useEffect(() => {
    
    const token = JSON.parse(localStorage.getItem('token'));

    if(token) {
      setIsLoggedIn(true)
      axios.get(`${masterUrl}${paths.me}`, {
        headers: {
          'Authorization': `Bearer ${token.access_token}`,
      },
      })
      .then(response => {
        setUserInfo(response.data)
      })
      .catch(err=> {throw new Error(err)})
    }

  }, [isLoggedIn]);

  return (
    <LoginContext.Provider value={{isLoggedIn, setIsLoggedIn, useInfo}}> 
    {/* truyền dữ liệu xuống component con bằng Context */}
      <div className="app">
        {/* Header */}
        <HeaderContainer/>
        {/* Body */}
        <BodyComponent/>
        {/* Footer */}
        <FooterComponent/>
      </div>
    </LoginContext.Provider>
  );
}

export default App;
