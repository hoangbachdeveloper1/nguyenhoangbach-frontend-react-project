const logoImage = { logo: require('./logo/Logo.png') };
const slideImage = require('./slide/banner2.jpg');
const paymentImg = require('./payment/payment.png');
const breadcrumb = require('./breadcrumb/breadcrumb.jpg');
const aboutListImg = [
    require('./about/img1.png'),
    require('./about/img2.png'),
    require('./about/img3.png'),
    require('./about/img4.png'),
];
const aboutPartnerImg = require('./about/logo-partner.png');
const aboutActivities = [
    require('./about/acti1.jpg'),
    require('./about/acti2.jpg'),
    require('./about/acti3.jpg'),
    require('./about/acti4.jpg'),
    require('./about/acti5.jpg'),
    require('./about/acti6.jpg'),
];
export { logoImage, slideImage, paymentImg, breadcrumb, aboutListImg, aboutPartnerImg, aboutActivities };
